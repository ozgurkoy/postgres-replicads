#!/bin/bash
SERVERS=()
SERVERS+=("q1")
SERVERS+=("q2")
for i in "${!SERVERS[@]}"
do
   	echo "SERVER  : ${SERVERS[$i]}"
	ssh -T root@${SERVERS[$i]} "/rop.sh status"
done
