#!/bin/bash
#admingledeployment firewall {{}} ---+++++
ips=()
#default ips
ips+=("78.186.62.81")
ips+=("192.168.0.0/23")
ips+=("10.0.0.0/22")
ips+=("10.211.55.2")
ips+=("10.211.55.4")
ips+=("192.34.60.112")
ips+=("88.248.141.157")
ips+=("108.61.191.223")
ips+=("31.168.158.55")
ips+=("85.25.209.17") #merkur

REGEX="^\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]"
APPSERVERREGEX="^\[(.+?)\]\[(.+?)\]"

for line in $(cat /root/replicads/conf/servers)
do
   if [[ $line =~ $REGEX ]]
   then
       hostname=${BASH_REMATCH[1]}
       typ=${BASH_REMATCH[2]}
       weight=${BASH_REMATCH[3]}
       ip=${BASH_REMATCH[4]}
	   ips+=($ip)
   fi
done

for line in $(cat /root/replicads/conf/appservers)
do
	if [[ $line =~ $APPSERVERREGEX ]]
	then
		ip=${BASH_REMATCH[1]}
		ips+=($ip)
	fi
done

#####################################################
# IPTables Firewall-Skript                          #
#                                                   #
# erzeugt mit dem IPTables-Skript-Generator auf     #
#      tobias-bauer.de - Version 0.4                #
# URL: http://www.tobias-bauer.de/iptables.html     #
#                                                   #
# Autor: Tobias Bauer                               #
# E-Mail: exarkun@ist-root.org                      #
#                                                   #
# Das erzeugte Skript steht unter der GNU GPL!      #
#                                                   #
# ACHTUNG! Die Benutzung des Skriptes erfolgt auf   #
# eigene Gefahr! Ich Ã¼bernehme keinerlei Haftung    #
# fÃ¼r SchÃ¤den die durch dieses Skript entstehen!    #
#                                                   #
#####################################################

# iptables suchen
iptables=`which iptables`

# wenn iptables nicht installiert abbrechen
test -f $iptables || exit 0

case "$1" in
   start)
      echo "Starte Firewall..."
      # alle Regeln lÃ¶schen
      $iptables -t nat -F
      $iptables -t filter -F
      $iptables -X

      # neue Regeln erzeugen
      $iptables -N garbage
      $iptables -I garbage -p TCP
      $iptables -I garbage -p UDP
      $iptables -I garbage -p ICMP

      # Default Policy
      $iptables -P INPUT DROP
      $iptables -P OUTPUT ACCEPT
      $iptables -P FORWARD DROP

      # Ã¼ber Loopback alles erlauben
      $iptables -I INPUT -i lo -j ACCEPT
      $iptables -I OUTPUT -o lo -j ACCEPT


      $iptables -A INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
      $iptables -A INPUT -m state --state NEW -p tcp --dport 443 -j ACCEPT


for i in "${ips[@]}"
do
	input="$iptables -I INPUT -i eth0 -p TCP --sport 1024:65535 --src ${i} -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT"
	$input
done



      #####################################################
      # Erweiterte Sicherheitsfunktionen
      # SynFlood
      $iptables -A FORWARD -p tcp --syn -m limit --limit 1/s -j ACCEPT
      # PortScan
      $iptables -A FORWARD -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s -j ACCEPT
      # Ping-of-Death
      $iptables -A FORWARD -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT

      #####################################################
      # bestehende Verbindungen akzeptieren
      $iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
      $iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

      #####################################################
      # Garbage Ã¼bergeben wenn nicht erlaubt
      $iptables -A INPUT -m state --state NEW,INVALID -j garbage

      #####################################################
      # alles verbieten was bisher erlaubt war
      $iptables -A INPUT -j garbage
      $iptables -A OUTPUT -j garbage
      $iptables -A FORWARD -j garbage
      ;;
   stop)
      echo "Stoppe Firewall..."
      $iptables -t nat -F
      $iptables -t filter -F
      $iptables -X
      $iptables -P INPUT ACCEPT
      $iptables -P OUTPUT ACCEPT
      $iptables -P FORWARD ACCEPT
      ;;
   restart|reload|force-reload)
   $0 stop
   $0 start
      ;;
   *)
      echo "Usage: /etc/init.d/firewall (start|stop)"
      exit 1
      ;;
esac
exit 0