#!/bin/bash
[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/$(basename $0)"; exit 1; }

export MAINLOG="/var/log/replicads/sync"
export DOINFOLOG=1

source def/vars.sh
source def/functions.sh

warningOutput "SYNCING CONF FILES WITH SERVERS"
for line in $(cat /root/replicads/conf/servers)
do
   if [[ $line =~ $REGEX ]]
   then
		host=${BASH_REMATCH[1]}
		typ=${BASH_REMATCH[2]}
		weight=${BASH_REMATCH[3]}
		ip=${BASH_REMATCH[4]}
		
		[[ $host == $SELFHOST ]] && continue;
		
		if [[ $(sshCheck $host) -eq 0 ]] ;then 
			errorOutput "SERVER ($host ) can't be reached for confsync"
			continue
		fi

		rsync -azh -e "ssh -o StrictHostKeyChecking=no" $CONFPATH/{appservers,servers,masters} root@$ip:$CONFPATH/
	fi
done