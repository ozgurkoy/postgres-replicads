portcheck() {
	if ! nc -w 5 -z $1 $2 2>/dev/null
	then
		echo "0"
	else
		echo "1"
		# exit
	fi
}

errorOutput () {
	MSG=$(printf "$1")
	
	echo "[E][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 1)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
}

warningOutput () {
	MSG=$(printf "$1")
	
	echo "[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 3)[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
}

infoOutput () {

	MSG=$(printf "$1")
	echo "$(tput setaf 2)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
	
	if  [[ ! -z $DOINFOLOG || (! -z $2 && $2 == "log") ]];then
		echo "[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	fi
}

recoveryQuery() {
	eff=`psql -t --username=repmgr --dbname=repmgr --host $1 -w -c "SELECT pg_is_in_recovery()"|& grep -P -i ^.+$`
	eff=$(echo $eff| sed -e 's/^ *//' -e 's/ *$//')
	
	echo $eff
}

replaceInFile(){
	host=$1
	file=$2
	replacement=$3
	keyplacement=$4
	[[ -z $4 ]] && keyplacement="replicads"
	
	# if grep -Fxq "#startreplicads" "$file"
	# then
	ssh $SSHOPT root@$host "[[ ! -f \"$file\" ]] && touch \"$file\";if grep -Fxq \"#start$keyplacement\" \"$file\"; then perl -0777 -i.original -pe \"s~#start$keyplacement.+?#end$keyplacement~#start$keyplacement\n$replacement\n#end$keyplacement~igs\" \"$file\"; else echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> \"$file\";fi"
	# else
	    # ssh $SSHOPT root@$host "echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> $file"
	# fi
}

isNumeric(){ 
	num=$1
	[ ! -z "${num##*[!0-9]*}" ] && echo 1 || echo 0
}

export -f portcheck
export -f errorOutput
export -f warningOutput
export -f infoOutput
export -f recoveryQuery
export -f replaceInFile
export -f isNumeric