SERVERS=()
POOLS=()
SSHOPT=" -T  -o StrictHostKeyChecking=no "
CONFIGPATH="/root/replicads/conf"
LOGPATH="/root/replicads/log"
CONFPATH="/root/replicads/conf"
FLAGPATH="/root/replicads/flags"
DAEMONLOG="/var/log/replicads/psql.log"
MAINLOG="/var/log/replicads/REPLICADS_NODE.log"
SELFHOST=$(hostname)
me=$(whoami)
