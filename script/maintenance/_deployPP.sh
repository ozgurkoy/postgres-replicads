#!/bin/bash
apt-get -y -q update
apt-get -y -q install git libpq-dev build-essential incron postgresql-9.3 postgresql-contrib-9.3 arping pgpool2 postgresql-9.3-pgpool2 msmtp openssl ca-certificates

echo "$1" > /etc/hostname
echo "$1" >> /proc/sys/kernel/hostname

echo "postgres:p123" | chpasswd
! grep -Pq "127.0.0.1[\s\t]+?$1$" /etc/hosts && echo -e "127.0.0.1\t$1" >> /etc/hosts

echo 'PATH=/usr/lib/postgresql/9.3/bin:$PATH' >> /root/.profile
echo 'PATH=/usr/lib/postgresql/9.3/bin:$PATH' >> /var/lib/postgresql/.profile

update-rc.d postgresql disable
service postgresql stop

echo "admin:`pg_md5 $2`" >> /etc/pgpool2/pcp.conf

sed -i.orig \
-e "s/^listen_addresses = .localhost./listen_addresses = '*'/" \
-e "s/^log_destination = .stderr./log_destination = 'syslog'/" \
-e "s/^port = .*/port = 5432/" \
-e "s/^log_hostname =.*/log_hostname = on/" \
-e "s/^syslog_facility =.*/syslog_facility = 'daemon.info'/" \
-e "s/^sr_check_user =.*/sr_check_user = 'admin'/" \
-e "s/^sr_check_password =.*/sr_check_password = '$2'/" \
-e "s/^health_check_period =.*/health_check_period = 10/" \
-e "s/^health_check_user =.*/health_check_user = 'admin'/" \
-e "s/^health_check_password =.*/health_check_password = '$2'/" \
-e "s/^netmask 255.255.255.0/netmask 255.255.255.0/" \
-e "s/^heartbeat_device0 =.*/heartbeat_device0 = 'eth0'/" \
-e "s/^#other_pgpool_port0 =.*/other_pgpool_port0 = 5432/" \
-e "s/^#other_wd_port0 = 9000/other_wd_port0 = 9000/" \
-e "s/^load_balance_mode = off/load_balance_mode = on/" \
-e "s/^master_slave_mode = off/master_slave_mode = on/" \
-e "s/^master_slave_sub_mode =.*/master_slave_sub_mode = 'stream'/" \
-e "s@^failover_command = ''@failover_command = '/root/replicads/script/failover_stream.sh %d %H'@" \
-e "s/^recovery_user = 'nobody'/recovery_user = 'admin'/" \
-e "s/^recovery_password = ''/recovery_password = '$2'/" \
-e "s/^recovery_1st_stage_command = ''/recovery_1st_stage_command = 'basebackup.sh' #do we need this?/" \
-e "s/^sr_check_period = 0/sr_check_period = 10/" \
-e "s/^delay_threshold = 0/delay_threshold = 10000000/" \
-e "s/^log_connections = off/log_connections = on/" \
-e "s/^log_statement = off/log_statement = on/" \
-e "s/^log_per_node_statement = off/log_per_node_statement = on/" \
-e "s/^log_standby_delay = 'none'/log_standby_delay = 'always'/" \
-e "s/^enable_pool_hba = off/enable_pool_hba = on/" \
/etc/pgpool2/pgpool.conf

echo "host    all         all         0.0.0.0/0             md5" >> /etc/pgpool2/pool_hba.conf

pg_md5 -m -u admin $2
chmod 6755 /sbin/ifconfig
chmod 6755 /usr/sbin/arping

chown -R postgres:postgres /var/lib/postgresql /etc/pgpool2/pool_passwd
ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

[[ ! -d /var/run/postgresql ]] && mkdir /var/run/postgresql
[[ ! -d /var/log/replicads ]] && mkdir /var/log/replicads && chmod 777 /var/log/replicads

(cd /root;git clone https://ozgurkoy@bitbucket.org/ozgurkoy/postgres-replicads.git;mv postgres-replicads replicads;cp replicads/template/replicads /etc/init.d/;rm flags/*;mkdir replicads/ppflags;)


service pgpool2 stop