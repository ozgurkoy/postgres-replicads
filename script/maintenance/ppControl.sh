#!/bin/bash
#todo appserver hostname fix
#todo[DONE] firewall

[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/maintenance/$(basename $0)"; exit 1; }
export MAINLOG="/var/log/replicads/sync"
export DOINFOLOG=1
source def/vars.sh
source def/functions.sh

echo `date +%s` > "$PPFLAGPATH/ping"

GOMASTER=1
FORCE=$1
IAMMASTER=$([[ -f $PPFLAGPATH/isMaster ]] && echo 1)
NODECONNECTION=$($REPPATH/script/pp.sh testNodeConnections)

if [[ $NODECONNECTION -eq 0 ]];then
	errorOutput "I CAN NOT CONNECT THE MIN RATIO OF NODES($MINCONNECTRATIO) TO CONTINUE"
	exit
fi

updateHostsFile(){
	NEWCONFIGSERVERIP="$1"
	
	HOSTS="\n$NEWCONFIGSERVERIP  $CONFHOSTNAME"
	
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $NODEREGEX ]];then
			host=${BASH_REMATCH[1]}
			weight=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[3]}
			if [[ $(sshCheck $ip) -eq 0 ]];then
				errorOutput "UPDATECONFIGHOST: NODE $hostname($ip) IS DOWN"
				continue
			fi
			
			replaceInFile "$host" "/etc/hosts" "$HOSTS" "REPCONHOST"
		fi
	done
	
	for line in $(cat $REPPATH/conf/appservers)
	do
	   if [[ $line =~ $APPSERVERREGEX ]]
	   then
	        ip=${BASH_REMATCH[1]}
	        hostname=${BASH_REMATCH[2]}
			if [[ $(sshCheck $ip) -eq 0 ]];then
				errorOutput "UPDATECONFIGHOST: APPSERVER $hostname($ip) IS DOWN"
				continue
			fi
			
			replaceInFile "$host" "/etc/hosts" "$HOSTS" "REPCONHOST"
		fi
	done
}

testThroughOther() {
	TARGET="$1"
	TYPE="$2"
	RETURN=""
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $NODEREGEX ]]
		then
			host=${BASH_REMATCH[1]}
			weight=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[3]}
			
			# warningOutput "PPCONTROL:TRYING CONNECTION OVER $host"
			
			if [[ $(sshCheck $host) -eq 0 ]] ;then
				continue
			fi
			
			if [[ $TYPE == "confCheck" ]];then
				RETURN=`ssh $SSHOPT root@$host "$REPPATH/script/rop.sh confHostIsReachable" |& grep -P -i ^.+$`
			else
				RETURN=`ssh $SSHOPT root@$host "nc -w 5 -z $TARGET 22 2>/dev/null && echo 1" |& grep -P -i ^.+$`
			fi
			
			break
		fi
	done
	
	echo $RETURN
}

runThroughOther() {
	TARGET="$1"
	COMMAND="$2"
	RETURN=0
	
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $NODEREGEX ]]
		then
			host=${BASH_REMATCH[1]}
			weight=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[3]}
			
			# warningOutput "PPCONTROL:TRYING CONNECTION OVER $host"
			
			if [[ $(sshCheck $host) -eq 0 ]] ;then
				continue
			fi
			
			RETURN=`ssh $SSHOPT root@$host "ssh $SSHOPT root@$TARGET '$COMMAND'" |& grep -P -i ^.+$`
			break
		fi
	done
	
	echo $RETURN
}

#considering that because of some error we might have multiple masters.
equateMasters() {
	FOUNDMASTERS=()
	HEAVIEST=0
	HEAVIESTPP=""
	DOACT=1

	[[ $IAMMASTER -eq 1 ]] && FOUNDMASTERS+=("$SELFHOST")
	
	
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $PPREGEX ]]
		then
			host=${BASH_REMATCH[1]}
			weight=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[3]}
			RUNTHROUGHOTHER=0
			# echo 123;exit
			
			#we are not skipping because we will check ourself too as comparison
			# [[ $host == $SELFHOST ]] && continue;
			
			if [[ $(sshCheck $host) -eq 0 ]] ;then
				otherConnection=$(testThroughOther "$host")
				if [[ $otherConnection -eq 1 ]];then
					declare "RT_$host=1"
					RUNTHROUGHOTHER=1
				else
					errorOutput "PPControl : $host IS NOT CONNECTABLE DIRECTLY OR INDIRECTLY"
					continue
				fi
			fi
			
			
			#if there's no flag then it's 
			if [[ $RUNTHROUGHOTHER -eq 0 ]];then
				ratio=$(ssh $SSHOPT root@$host "(cd /root/replicads;script/pp.sh testNodeConnections)")
				isMaster=$(ssh $SSHOPT root@$host "[[ -f \"$PPFLAGPATH/isMaster\" ]] && echo 1")
				pstatus=$(daemonRunning "$host")
				gotKill=$(ssh $SSHOPT root@$host "[[ -f \"$PPFLAGPATH/killPP\" ]] && echo 1")
			else
				ratio=$(runThroughOther "$host" "(cd /root/replicads;script/pp.sh testNodeConnections)")
				isMaster=$(runThroughOther "$host" "[[ -f '$PPFLAGPATH/isMaster' ]] && echo 1")
				pstatus=$(daemonRunning "$host" tunnel)
				gotKill=$(runThroughOther "$host" "[[ -f \"$PPFLAGPATH/killPP\" ]] && echo 1")
			fi
						
			if [[ $ratio -ne 1 ]];then
				errorOutput "PPControl : $host IS NOT INDIRECTLY CONNECTABLE, BUT CONNECTABLE RATIO IS NOT ENOUGH(LOWER THAN $MINCONNECTRATIO)"
				continue
			fi
			
			#this server is heavier than me
			if [[ $weight -gt $SELFWEIGHT && $gotKill -ne 1 ]];then
				DOACT=0
			fi
			
			if [[ $isMaster -eq 1 ]];then
				infoOutput "$host IS A MASTER"
				FOUNDMASTERS+=("$host")
				declare "MASTER_$host=1"
			fi

			if [[ $weight -gt $HEAVIEST && $gotKill -ne 1 ]];then
				#if both servers are masters either it will die 
				#the other master is heavier. we are skipping being master now.
					HEAVIEST=$weight
					HEAVIESTPP=$host
					# infoOutput "$host is heavier than me. We are giving away being master."
			fi
			
		fi
		
	done
	
	#now check if there's only one master.
	if [[ "${#FOUNDMASTERS[@]}" -eq 1 ]];then
		infoOutput "SINGLE MASTER FOUND. THAT'S GOOD"
		
		if [[ $FORCE != "force" ]];then
			infoOutput "SINCE THE 'force' PARAMETER WAS NOT USED WE WONT BE TOUCHING."
			return
		fi
	fi
	

	for i in "${!FOUNDMASTERS[@]}"
	do
		hasTunnel="RT_${FOUNDMASTERS[$i]}";
		
		if [[ "${FOUNDMASTERS[$i]}" == "$HEAVIESTPP" ]];then
			#this is the master, skipping
			continue;
		fi		
		
		if [[ -z ${!hasTunnel} ]];then
			infoOutput "KILLING ${FOUNDMASTERS[$i]} DIRECTLY"
			ssh $SSHOPT root@${FOUNDMASTERS[$i]} "rm \"$PPFLAGPATH/isMaster\";touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;"
		else	
			infoOutput "KILLING ${FOUNDMASTERS[$i]} (IN)DIRECTLY"
			$(runThroughOther "${FOUNDMASTERS[$i]}" "rm \"$PPFLAGPATH/isMaster\";touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;" )
		fi
	done;
	
	if [[ "${#FOUNDMASTERS[@]}" -eq 0 ]];then
		warningOutput "SHIEET.NO MASTERS FOUND"
	fi

	gmaster="MASTER_${HEAVIESTPP}";
	
	if [[ $DOACT -eq 0 && $FORCE != "force" ]];then
		infoOutput "WE WONT BE ACTING BECAUSE THERE'S A HEAVIER SERVER THAN ME ONLINE. SHE'LL DO WHATS NECESSARY"
		return
	fi

	if [[ $HEAVIESTPP == "" ]];then
		errorOutput "NO HEAVY PP FOUND!!"
		
		return
	fi


	if [[ -z ${!gmaster} ]];then
		hasTunnel="RT_$HEAVIESTPP";
		infoOutput "HEAVIEST($HEAVIESTPP) NOT A MASTER, PROMOTING NOW "
		if [[ -z ${!hasTunnel} ]];then
			ssh $SSHOPT root@$HEAVIESTPP "echo `date +%s` > $PPFLAGPATH/isMaster;touch $PPFLAGPATH/restartServers;touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;"
		else
			$(runThroughOther "$HEAVIESTPP" "echo `date +%s` > $PPFLAGPATH/isMaster;touch $PPFLAGPATH/restartServers;touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;" )
		fi
		
		# ssh $SSHOPT root@$HEAVIESTPP "echo `date +%s` > $PPFLAGPATH/isMaster;touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;"
		
		
		###########
		
		# HERE
		
		###########
	else
		infoOutput "HEAVIEST $HEAVIESTPP IS ALREADY A MASTER"
	fi
	
	
	return
	# exit;
	
	
	echo ${FOUNDMASTERS[@]}
	exit
	rm "$PPFLAGPATH/isMaster"
	ssh $SSHOPT root@$host "echo `date +%s` > $PPFLAGPATH/isMaster;touch $PPFLAGPATH/restartPP;/etc/init.d/replicads restart;"
	updateHostsFile "$ip"
	IAMMASTER=0
	
}

daemonRunning() {
	THROUGH=$2
	if [[ $(sshCheck $1) -eq 0 ]] ;then
		echo 0
		return 0
	fi
    
	if [[ $THROUGH == "tunnel" ]];then
		serverDaemonStatus=$(runThroughOther "$host" 'pstatus=$(/etc/init.d/replicads status);echo $pstatus')
	else
		serverDaemonStatus=`ssh $SSHOPT root@$1 "/etc/init.d/replicads status" |& grep -P -i ^.+$`
	fi
	
	start=$(echo "$serverDaemonStatus"|sed 's/.*\(is running\).*/good/')

	if [[ "$start" == "good" ]]; then
		echo 1
	else
		echo 0
	fi
}

execute() {
	#find other pps
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $PPREGEX ]]
		then
			host=${BASH_REMATCH[1]}
			weight=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[3]}

			[[ $host == $SELFHOST ]] && continue;

			infoOutput "PPControl : PPSync with $host"

			isMaster=$(ssh $SSHOPT root@$host "[[ -f \"$PPFLAGPATH/isMaster\" ]] && echo 1")
			DAEMONSTATUS=$(daemonRunning "$ip")
			
			if [[ $isMaster -eq 1 ]];then
				infoOutput "PPControl : $host is the master. Checking for status"
				GOMASTER=0
				
				if [[ $DAEMONSTATUS -eq 1 ]];
				then
					#since this is the master we are good
					infoOutput "PPControl : $host status is good as master."
					return
				else
					infoOutput "PPControl : $host status is not good as master. Daemon is not running. Trying to ignite"
					ssh $SSHOPT root@$host "/etc/init.d/replicads restart;"
				fi
			fi

			# CHECK PING TIMES HERE


			if [[ $(sshCheck $host) -eq 0 ]] ;then
				errorOutput "PPControl : PP ($host ) can't be reached for pp control, trying over other servers"
				otherConnection=$(testThroughOther "$host")

				if [[ $weight -gt $SELFWEIGHT && $otherConnection -eq 1 ]];then
					warningOutput "PPControl : This($host) is a heavier host. We can't reach it but others can. we are skipping for now"
					GOMASTER=0
					return
				else
					warningOutput "PPControl : Can't reach the other host($host) through other servers so we are skipping."
					continue
				fi
			else
				#we can connect and it's bigger
				if [[ $weight -gt $SELFWEIGHT ]];then
					infoOutput "PPControl : $host is bigger and online so we are skipping"
					GOMASTER=0
					return
				# else
					#we can go master
					# GOMASTER=1
				fi
			fi
		fi
	done
}

if [[ $IAMMASTER -eq 1 || $1 == "force" ]]; then
	if [[ $1 == "force" ]];then
		infoOutput "Forcing equate"
	else
		infoOutput "PPControl : I am the master."
	fi
	equateMasters
	#well i am the master pp, but are there others?
else
	infoOutput "I AM NOT THE MASTER."
	equateMasters
	exit
	execute
	echo "GO MASTER VALUE IS $GOMASTER"
	if [[ $GOMASTER -eq 1 && ! -f $PPFLAGPATH/killPP ]];then
		echo "GOING GOING"
#		go master
		# touch $FLAGPATH/restartDaemon
		# echo "$PPFLAGPATH/isMaster"
		touch "$PPFLAGPATH/restartPP"
		/etc/init.d/replicads restart
		echo `date +%s` > "$PPFLAGPATH/isMaster"
		updateHostsFile "$ip"
	fi
	
	#temporary action below
 	equateMasters
fi
