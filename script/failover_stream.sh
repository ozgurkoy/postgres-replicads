#!/bin/bash
# Failover command for streaming replication.
#
# Arguments: $1: failed node id. $2: new master hostname.

failed_node=$1
new_master=$2
/root/replicads/sh/pp.sh "urgentError" "!!! Failover Triggered - failed node : $failed_node !!!"
/root/replicads/sh/pp.sh "checkNodes"
exit
if [[ -f /replicads/nofailover ]];then
	echo "AN OPERATION IS GOING ON, CAN'T RUN FAILOVER NOW" >> /replicads/failover_stream.log
	exit
fi

portcheck() {
        if ! nc -w 5 -z $1 $2 2>/dev/null
        then
                echo "0"
        else
                echo "1"
                # exit
        fi
}

(

if [[ ${#new_master} -eq 0 ]]; then
	echo "NO NEW MASTER FOUND " >> /replicads/failover_stream.log
	exit
fi

/replicads/daemon.sh clearFlags
#we must decide if the one gone is the master.
#check last master.
lastMaster=$(/replicads/daemon.sh loggedMaster)
if [[ "$lastMaster" == "$failed_node" ]];then
	/replicads/daemon.sh putFlag "$failed_node" "clone" #"kill"
else
	/replicads/daemon.sh putFlag "$failed_node" "clone"
fi
date '+%s' >  /replicads/pstats/daemonop

echo "Failed node: $failed_node $new_master"
set -x
if [[ $(portcheck $new_master 22) -eq 0 ]];then
	#new master is down.
	echo "NEW MASTER CAN'T BE REACHED $new_master"
else
	/usr/bin/ssh -T -l root $new_master "/replicads/rop.sh promoteforce"
fi

[[ -f /replicads/pstats/daemonop ]] && rm /replicads/pstats/daemonop
 

 
exit 0;
) 2>&1 | tee -a /replicads/failover_stream.log
