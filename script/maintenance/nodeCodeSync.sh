#!/bin/bash
[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/$(basename $0)"; exit 1; }

export MAINLOG="/var/log/replicads/sync"
export DOINFOLOG=1

source def/vars.sh
source def/functions.sh
infoOutput "STARTING CODE SYNC" "log"
for line in $(cat /root/replicads/conf/servers)
do
   if [[ $line =~ $NODEREGEX ]]
   then
        match=${BASH_REMATCH[3]}

		if [[ $(sshCheck $match) -eq 0 ]];then
			errorOutput "CANNOT REACH $match for CODESYNC"
			continue
		fi
		
		
        if [[ $SELF != $match ]]
        then
            infoOutput  "Updating SCRIPT on $match"
			rsync -azh -e "ssh" $REPPATH/node/script  root@$match:$REPPATH/
            infoOutput  "Updating CONF on $match"
			rsync -azh -e "ssh" $REPPATH/conf  root@$match:$REPPATH/
            infoOutput  "Updating INIT SCRIPT on $match"
			rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/replicads  root@$match:/etc/init.d/replicads
			infoOutput "PUTTING RESTART DAEMON FLAG ON $match"
			# ssh $SSHOPT root@$match "touch $FLAGPATH/restartDaemon"
			ssh $SSHOPT root@$match "/etc/init.d/replicads restart" 
			
        fi
   fi
done
