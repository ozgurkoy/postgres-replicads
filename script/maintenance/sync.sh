#!/bin/bash
#todo appserver hostname fix
#todo[DONE] firewall

[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/maintenance/$(basename $0)"; exit 1; }
export MAINLOG="/var/log/replicads/sync"
export DOINFOLOG=1
#temporary
PPASS="password123"
source def/vars.sh
source def/functions.sh
declare -A ACTIVEREPNODES

POOLCOUNT=0
NODECOUNT=1
INPARAM="$1"

syncOpFlag(){
	SPECIFIC="$2"
	for line in $(cat $REPPATH/conf/servers)
	do
		if [[ $line =~ $REGEX ]]
		then
			hostname=${BASH_REMATCH[1]}
			typ=${BASH_REMATCH[2]}
			ip=${BASH_REMATCH[4]}
			
			[[ ${#SPECIFIC} -gt 0 && $hostname != $SPECIFIC ]] && continue
			
			if [[ $typ == "pp" ]];then
				FP=$PPFLAGPATH
			else
				FP=$FLAGPATH
			fi
			curd=$(date '+%s')
			
			if [[ $1 == "put" ]];then
				infoOutput "PUTTING SYNCOP FLAG ON $hostname"
				ssh $SSHOPT root@$ip "echo $curd > $FP/syncop"
			else
				infoOutput "REMOVING SYNCOP FLAG ON $hostname"
				ssh $SSHOPT root@$ip "[[ -f $FP/syncop ]] && rm $FP/syncop"
			fi
		fi
	done
}

checkPPConfig(){
	PPWEIGHTS=()
	GOTPP=0
	SERVERNAMES=""
	for line in $(cat $REPPATH/conf/servers)
	do
		if [[ $line =~ $REGEX ]]
		then
			hostname=${BASH_REMATCH[1]}
			typ=${BASH_REMATCH[2]}
			weight=${BASH_REMATCH[3]}
			ip=${BASH_REMATCH[4]}
			
			if echo $SERVERNAMES|grep -Fq "~$hostname~" ;then
				errorOutput "$hostname HAS BEEN DEFINED MORE THAN ONCE"
				exit
			fi
			
			SERVERNAMES="$SERVERNAMES~$hostname~"
			
			[[ $typ == "node" ]]  && continue
			GOTPP=1
			if [[ ! -z ${PPWEIGHTS[$weight]} ]];then
				errorOutput "CAN'T START SYNC, TWO PP SERVERS HAVE THE SAME WEIGHT ${PPWEIGHTS[$weight]}  - $hostname"
				exit
			fi
		
			PPWEIGHTS[$weight]=$hostname

			if [[ -f "$FLAGPATH/activenode-$hostname" ]]; then
				# echo $FLAGPATH/activenode-$hostname
				ssh $SSHOPT root@$ip "[[ -f /etc/init.d/firewall ]] && /etc/init.d/firewall stop"
			fi
		fi
	done
	if [[  $GOTPP -eq 0 ]];then
		errorOutput "CAN'T START SYNC, NO PP IS DEFINED"
		exit
	fi
}

confSync() {
	host=$1
	rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/conf  root@$host:$REPPATH
}

flagSync() {
	host=$1
	rsync -azh -e "ssh -o StrictHostKeyChecking=no" $FLAGPATH  root@$host:$REPPATH/
}

firewallOff() {
	for line in $(cat $REPPATH/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        hostname=${BASH_REMATCH[1]}
	        typ=${BASH_REMATCH[2]}
	        weight=${BASH_REMATCH[3]}
	        ip=${BASH_REMATCH[4]}

			if [[ $(sshCheck $ip) -eq 0 ]];then
				errorOutput "Firewall off : $hostname($ip) IS DOWN"
				continue
			fi
			
			if [[ -f "$FLAGPATH/activenode-$hostname" ]]; then
				# echo $FLAGPATH/activenode-$hostname
				ssh $SSHOPT root@$ip "[[ -f /etc/init.d/firewall ]] && /etc/init.d/firewall stop"
			fi
		fi
	done
		
}

poolConf(){
	replacement=$1
	
	newFile="/tmp/pgpool.conf.$(date +'%y%m%d%H%M%S')"
	cp $REPPATH/template/pgpool.conf $newFile
	perl -0777 -i.original -pe 's~^\s+?backend_[a-z_]+?[0-9].+?=.*$~~igm' $newFile
	chown postgres:postgres $newFile
	replaceInFile "$SELFHOST" "$newFile" "$replacement"
	
	for i in "${!POOLS[@]}"
	do
		infoOutput "SYNCING POOL.CONF WITH ${POOLS[$i]}"
		rsync -azh -e "ssh -o StrictHostKeyChecking=no" $newFile root@${POOLS[$i]}:/etc/pgpool2/pgpool.conf
		# exit
	done
	
	
	replaceInFile "$host" "pool.conf" "$replacement"
}

pgpassConf(){
	replacement=$1
	
	for i in "${!DBRELATED[@]}"
	do
		infoOutput "SYNCING PGPASS WITH ${DBRELATED[$i]}"
		replaceInFile "${DBRELATED[$i]}" "/root/.pgpass" "$replacement"
		replaceInFile "${DBRELATED[$i]}" "/var/lib/postgresql/.pgpass" "$replacement"
		
		ssh $SSHOPT root@${DBRELATED[$i]} "chmod 600 /root/.pgpass /var/lib/postgresql/.pgpass;chown postgres:postgres /var/lib/postgresql/.pgpass"
	done
}

hbaConf(){
	replacement=$1
	
	newFile="/tmp/pg_hba.conf.$(date +'%y%m%d%H%M%S')"
	cp $REPPATH/template/pg_hba.conf $newFile
	chown postgres:postgres "$newFile"
	
	replaceInFile "$SELFHOST" "$newFile" "$replacement"
	# return
	# rsync with nodes
	for i in "${!NODES[@]}"
	do
		infoOutput "SYNCING HBA WITH ${NODES[$i]}"
		rsync -azh -e "ssh -o StrictHostKeyChecking=no" $newFile  root@${NODES[$i]}:/data/pg_hba.conf
		ssh $SSHOPT root@$host "/etc/init.d/replicads restart" 
		# exit
	done
        # echo "Updating conf on $match"
}

getMaxNodeId(){
	eff=$(psql -t -U repmgr -h $lastMaster -d repmgr -w -c "select max(id)+1 from repmgr_pgsql_cluster.repl_nodes")
	echo $eff
}

syncRepmgrConf(){
	infoOutput "SYNCING REPMGR CONF"
	TRYCOUNT=1
	CONNECTED=0
	
    while [ "${CONNECTED}" -eq 0 ];do
		LASTM=$($REPPATH/script/pp.sh loggedMaster | tail -n1)
		if [[ $(portcheck $LASTM 5432) -eq 0 ]];then
			echo "PSQL CONNECTION ERROR TO LAST MASTER($LASTM), WILL TRY AGAIN" 
			TRYCOUNT=$((TRYCOUNT+1))
			sleep 5
		else
			CONNECTED=1
		fi
		
		if [[ $TRYCOUNT -gt 5 ]];then
			echo "PSQL CONNECTION ERROR TO LAST MASTER($LASTM), TRIED 5 TIMES, GIVING UP" 
			return
		fi
	done
	
	eff=$(psql -t -U repmgr -h $LASTM -d repmgr -w -c "select id,name from repmgr_pgsql_cluster.repl_nodes")
	# echo -e $eff
	# 	eff="  1 | q1
	#   2 | q2
	#   3 | q3
	#   4 | q10
	# "
	arrIN=${eff// /}
	arrIN=(${arrIN//\$/ })
	NODEINDEX=""
	NODESTRING=""
	
	#check for duplicate records first.
	for i in "${!arrIN[@]}"
	do
		ll=${arrIN[$i]}
		arrIN2=(${ll//|/ })
		lnumber=$(findServerLine ${arrIN2[1]} "node")
		
		if echo "$NODESTRING" | grep -q "|${arrIN2[1]}|" && [ $lnumber -ne ${arrIN2[0]} ]
		then
			errorOutput "${arrIN2[1]} IS DEFINED MORE THAN ONCE IN THE REPMGR DB. REMOVING THE NON GOOD ONE ( ${arrIN2[0]} )"
		
			eff=$(psql -t -U repmgr -h $LASTM -d repmgr -w -c "\
				delete from repmgr_pgsql_cluster.repl_nodes where id='${arrIN2[0]}';")
		
			continue
		fi
	
		
		NODESTRING="$NODESTRING|${arrIN2[1]}|"
	done
	
	NODESTRING=""
	for i in "${!arrIN[@]}"
	do
		ll=${arrIN[$i]}
		arrIN2=(${ll//|/ })
		ACTIVEREPNODES[${arrIN2[0]}]=${arrIN2[1]}
		# lnumber=$(grep -Pn \\[${arrIN2[1]}\\] $REPPATH/conf/servers|grep -Po ^[0-9]+)
		lnumber=$(findServerLine ${arrIN2[1]} "node")
		if [[ $lnumber -gt 0 ]];then
			echo "$lnumber >> ${arrIN2[0]} ${arrIN2[1]}"
			# continue
			# echo $NODESTRING ++ "|${arrIN2[1]}|"
			NODESTRING="$NODESTRING|${arrIN2[1]}|"
			
			echo "${arrIN2[1]} ${arrIN2[0]} $lnumber"
			
			#line numbers are equal
			[[ ${arrIN2[0]} -eq $lnumber ]] && continue;
			#this was defined before
			echo $NODEINDEX | grep -q "${arrIN2[0]}|" && continue
							
			le=($(readServerLine $lnumber "node")) 
			fe=($(readServerLine ${arrIN2[0]} "node"))

			errorOutput "${arrIN2[1]} :: REAL LINE:${arrIN2[0]} CURRENT LINE:$lnumber NOT MATCHING"
			warningOutput "CHANGING LINES FOR ${arrIN2[1]} ON REPMGR DB"
			NODEINDEX="$NODEINDEX|${arrIN2[0]}|$lnumber|"
			eff=$(psql -t -U repmgr -h $LASTM -d repmgr -w -c "\
				update repmgr_pgsql_cluster.repl_nodes set id=999 where id=$lnumber;\
				update repmgr_pgsql_cluster.repl_nodes set id=$lnumber where id=${arrIN2[0]};\
				update repmgr_pgsql_cluster.repl_nodes set id=${arrIN2[0]} where id=999;")
		
			echo "${fe[3]} ${fe[0]} ${arrIN2[0]} <<1"
			echo "${le[3]} ${le[0]} $lnumber <<2"
			
			repmgrConf ${fe[3]} ${fe[0]} ${arrIN2[0]}
			
			if [[ ${#le[0]} -gt 0 ]];then
				repmgrConf ${le[3]} ${le[0]} $lnumber
			fi
			# echo -e ${arrIN2[0]} ${arrIN2[1]}
			# echo "========"
		else
			warningOutput "DELETING ${arrIN2[1]} FROM REPMGR DB, IT'S NOT DEFINED HERE"
			#does not exist
			eff=$(psql -t -U repmgr -h $LASTM -d repmgr -w -c "\
				delete from repmgr_pgsql_cluster.repl_nodes where name='${arrIN2[1]}';")
		fi
	done

	for line in $(cat $REPPATH/conf/servers)
	do
	    if [[ $line =~ $NODEREGEX ]]
	    then
	        hostname=${BASH_REMATCH[1]}
			 
			echo "$NODESTRING" | grep -q "|$hostname|" && continue
			 
			errorOutput "$hostname IS NOT REGISTERED ON REPMGR"
			echo "registerStandby" > "$FLAGPATH/$hostname"
		fi
	done
	
	return 
	
	
	# exit
	for i in "${!ACTIVEREPNODES[@]}"
	do
		echo $i"--"${ACTIVEREPNODES[$i]}
		echo "====="
	done
	# exit
	IFS=$old
	abc=(${eff//$\\n/ })
	echo ${abc[1]}
	exit
	echo "${#abc[@]}"
	# echo $abc
	# exit
	# echo -e $eff
	# exit
	for i in "${!abc[@]}"
	do
		echo -e ${abc[$i]}
		echo "========"
	done
	
}

repmgrConf(){
	# rsync with nodes
	host=$1
	hostname=$2
	nodeId=$3
	
	newFile="repmgr.conf.$host.$(date +'%y%m%d%H%M%S')"
	cp $REPPATH/template/repmgr.conf /tmp/$newFile
	sed -i "s@\[node\]@$hostname@" /tmp/$newFile
	sed -i "s@\[nodeid\]@$nodeId@" /tmp/$newFile
	chown postgres:postgres "/tmp/$newFile"
	infoOutput "SYNCING REPMGR WITH $host"
	
	
	if [[ $(sshCheck $host) -eq 0 ]];then
		errorOutput "$hostname($host) IS DOWN"
		continue
	fi
	
	ssh $SSHOPT root@$host "[[ ! -d /var/lib/postgresql/repmgr ]] && mkdir /var/lib/postgresql/repmgr;chown -R postgres:postgres /var/lib/postgresql/repmgr"
	rsync -azh -e "ssh -o StrictHostKeyChecking=no -q" --backup --suffix=-`date +%F-%T` /tmp/$newFile  root@$host:/var/lib/postgresql/repmgr/repmgr.conf
	#this is unnecessary
	ssh $SSHOPT root@$host "chown -R postgres:postgres /var/lib/postgresql/repmgr/repmgr.conf"
}


	
for line in $(cat $REPPATH/conf/servers)
do
    if [[ $line =~ $REGEX ]]
    then
         ip=${BASH_REMATCH[4]}
		 [[ $(validIp $ip) -eq 0 ]] && errorOutput "$ip IS NOT A VALID IP. PLEASE CHECK." && exit
	fi
done

for line in $(cat $REPPATH/conf/appservers)
do
	if [[ $line =~ $APPSERVERREGEX ]]
	then
		ip=${BASH_REMATCH[1]}
		hostname=${BASH_REMATCH[2]}
		
	 	[[ $(validIp $ip) -eq 0 ]] && errorOutput "APPSERVER $ip IS NOT A VALID IP. PLEASE CHECK." && exit
		
	fi
done
exit

#START EXECUTION
checkPPConfig
lastMaster=$($REPPATH/script/pp.sh loggedMaster | tail -n1)
# syncRepmgrConf
# exit
MAXNODEID=$(getMaxNodeId)

[[ $(portcheck $lastMaster 5432) -eq 0 ]] && echo "PSQL CONNECTION ERROR TO LAST MASTER($lastMaster)" && $INPARAM != "force" && exit
firewallOff
# syncRepmgrConf
# exit
syncOpFlag "put"

#todo check if such a file exists
rm $FLAGPATH/activenode*

for line in $(cat $REPPATH/conf/servers)
do
   if [[ $line =~ $REGEX ]]
   then
        hostname=${BASH_REMATCH[1]}
        typ=${BASH_REMATCH[2]}
        weight=${BASH_REMATCH[3]}
        ip=${BASH_REMATCH[4]}
		
		HOSTS="$HOSTS\n$ip  $hostname"
		PGPASSCONF="$PGPASSCONF
$hostname:5432:postgres:admin:$PPASS
$hostname:5432:repmgr:repmgr:repmgr_password"

		if [[ $typ == "node" ]];then
			HBACONF="$HBACONF
host    all             postgres         $hostname        trust
host    repmgr          repmgr          $hostname        trust
host    replication     repmgr          $hostname        trust
host    repmgr          postgres          $hostname        trust
host    replication     postgres          $hostname        trust
host    all             postgres         $ip/32        trust
host    repmgr          repmgr          $ip/32        trust
host    replication     repmgr          $ip/32        trust
host    repmgr          postgres          $ip/32        trust
host    replication     postgres          $ip/32        trust
"
		elif [[ $typ == "pp" ]]; then
			HBACONF="$HBACONF
host    all             all           $hostname               md5
host    all             all           $ip/32               md5
"
		fi
		
		if [[ $(portcheck $ip 22) -eq 0 ]];then
			errorOutput "$hostname($ip) IS DOWN"
			continue
		fi
		
		warningOutput "COPYING $hostname ROOT  KEY TO SELF"
		#clear previous entries, probably will be required only for test states
		ssh-keygen -R $ip
		ssh-keygen -R $hostname
		
		ssh-copy-id -o "StrictHostKeyChecking no" root@$ip
		if [[ $ip != "$SELFIP" ]]; then
			ssh -T root@$ip "ssh-copy-id -o StrictHostKeyChecking=no root@$SELFIP"
		fi
		
		# We must install the node if necessary here.
		# check if installed
		installStatus=$(ssh $SSHOPT root@$ip "[[ ! -d \"/root/replicads\" ]] && echo \"not\"")
		if [[ $typ == "node" ]];then
			if [[ $installStatus == "not" ]]; then
				#run install script
				ssh -T root@$ip "bash -s " < $REPPATH/script/maintenance/_deployNode.sh $hostname
				confSync $ip
				repmgrConf $ip $hostname $MAXNODEID
				
				infoOutput "UPDATING POSTGRESQL.CONF ON $ip"
				rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/postgresql.conf  root@$ip:/data/postgresql.conf
				ssh $SSHOPT root@$ip "chown -R postgres:postgres /data/postgresql.conf"

				#transfer node files
				# rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/node/*  root@$ip:$REPPATH/
				# rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/conf  root@$ip:$REPPATH/
				NEWINSTALL+=($ip)
				echo "registerStandby" > "$FLAGPATH/$hostname" #left flag for registration
			else
				repmgrConf $ip $hostname $NODECOUNT
			fi
			NODECOUNT=$((NODECOUNT+1))
			MAXNODEID=$((MAXNODEID+1))
			# warningOutput "COPYING POSTGRES KEY TO $hostname"
			# ssh-copy-id postgres@$ip
		elif [[ $typ == "pp" && $installStatus == "not" ]]; then
			infoOutput "DEPLOYING PP ON $ip"
			ssh -T root@$ip "bash -s " < $REPPATH/script/maintenance/_deployPP.sh $hostname "$PPASS"
			confSync $ip
			flagSync $ip
			NEWINSTALL+=($ip)
		fi
		
		if [[ $hostname != $SELFHOST ]];then
			warningOutput "SYNCING SSH WITH $hostname"
			# scp -r /root/{.ssh,.pgpass} root@$hostname:/root/
			rsync -azh -e "ssh -o StrictHostKeyChecking=no" /root/.ssh root@$ip:/root/
			rsync -azh -e "ssh -o StrictHostKeyChecking=no" /root/.ssh root@$ip:/var/lib/postgresql/
			ssh $SSHOPT root@$ip "chown -R postgres:postgres /var/lib/postgresql/.ssh"
			ssh $SSHOPT root@$ip "service ssh restart"
		fi

		ALLSERVERS+=($ip)
		if [[ $typ == "node" ]];then
			# confSync $ip
						
			POOLCONF="$POOLCONF
backend_hostname$POOLCOUNT = '$hostname'
backend_port$POOLCOUNT = 5432
backend_weight$POOLCOUNT = $weight
backend_data_directory$POOLCOUNT = '/data'
backend_flag$POOLCOUNT = 'ALLOW_TO_FAILOVER'
"
			
			
			POOLCOUNT=$((POOLCOUNT+1))
			NODES+=($hostname)
			DBRELATED+=($ip)

			infoOutput "UPDATING REPLICADS SCRIPT ON $ip"
			rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/replicads  root@$ip:/etc/init.d/replicads
			ssh $SSHOPT root@$ip "chmod 755 /etc/init.d/replicads;update-rc.d replicads defaults 99;/etc/init.d/replicads restart;"

		elif [[ $typ == "pp" ]];then
			POOLS+=($hostname)
			DBRELATED+=($ip)
			ssh $SSHOPT root@$ip "chmod 755 /etc/init.d/replicads;update-rc.d replicads defaults 99;/etc/init.d/replicads restart;"
		fi
		
		
		confSync $ip
		
		infoOutput "UPDATING FIREWALL ON $ip"
		
		rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/firewall.sh  root@$ip:/etc/init.d/firewall
		rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/msmtprc  root@$ip:/root/.msmtprc
		ssh $SSHOPT root@$ip "chmod 755 /etc/init.d/firewall;update-rc.d firewall defaults 98;/etc/init.d/firewall restart;"
		
        # if [[ $SELF != $match ]]
        # then
			# echo $ip
			
            # echo "Updating conf on $match"
			# rsync -avzh -e "ssh" --backup --suffix=-`date +%F-%T` $REPPATH/conf  root@$match:$REPPATH/
        # fi
   fi
done

#we sync here. because all the servers will be deployed. and we can change the wrong id 

(cd $REPPATH && script/maintenance/nodeCodeSync.sh)

for line in $(cat $REPPATH/conf/appservers)
do
   if [[ $line =~ $APPSERVERREGEX ]]
   then
        ip=${BASH_REMATCH[1]}
        hostname=${BASH_REMATCH[2]}
		if [[ $(sshCheck $ip) -eq 0 ]];then
			errorOutput "$hostname($ip) IS DOWN"
			continue
		fi
		
		warningOutput "COPY ING ROOT KEY TO $ip"
		ssh-copy-id root@$hostname
		ssh -T root@$ip "ssh-copy-id root@$SELFIP"
		
		
		ALLSERVERS+=($ip)
		HOSTS="$HOSTS\n$ip $hostname"
		HBACONF="$HBACONF
host    all             all           $hostname               md5"
   fi
done


for i in "${!ALLSERVERS[@]}"
do
	infoOutput "CHANGING HOSTS FILE ON ${ALLSERVERS[$i]}"
	replaceInFile "${ALLSERVERS[$i]}" "/etc/hosts" "$HOSTS"
	# infoOutput "UPDATING FIREWALL ON ${ALLSERVERS[$i]}"
	# rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/firewall.sh  root@$host:/etc/init.d/firewall
	# ssh root@${ALLSERVERS[$i]} "chmod 755 /etc/init.d/firewall;update-rc.d firewall defaults;/etc/init.d/firewall restart;"
	cat $REPPATH/template/motd | ssh root@${ALLSERVERS[$i]} "cat > /etc/motd"
	
done

# touch "$PPFLAGPATH/restartServers"
# touch "$PPFLAGPATH/restartPP"
infoOutput "LEFT FLAGS FOR RESTART SERVER&PP"

hbaConf "$HBACONF"
poolConf "$POOLCONF"
pgpassConf "$PGPASSCONF"

for i in "${!NEWINSTALL[@]}"
do
	# infoOutput "CLONING&REGISTERING NEW SERVER ${NEWINSTALL[$i]} ON MASTER"
	infoOutput "STARTING DAEMON ON ${NEWINSTALL[$i]}"
	ssh $SSHOPT root@${NEWINSTALL[$i]} "/etc/init.d/replicads restart"
	# ssh $SSHOPT root@${NEWINSTALL[$i]} "su - postgres -c \"repmgr -f /var/lib/postgresql/repmgr/repmgr.conf --verbose master register\""
	
	# echo "clone~">"$FLAGPATH/${NEWINSTALL[$i]}"
done

for i in "${!NODES[@]}"
do
	infoOutput "ACTIVATING NODE: ${NODES[$i]}"
	#todo : check if it's a new node , we must check and not put the flag for startup
	touch "$FLAGPATH/activenode-${NODES[$i]}"
done

#removing last masters flag so that the repmgr functions will work
syncOpFlag "UNPUT" "$lastMaster"

syncRepmgrConf

(cd /root/replicads;script/maintenance/ppsync.sh;script/maintenance/ppControl.sh force)

syncOpFlag

exit

for line in $(cat $REPPATH/conf/servers)
do
   if [[ $line =~ $REGEX ]]
   then
        hostname=${BASH_REMATCH[1]}
        typ=${BASH_REMATCH[2]}
        weight=${BASH_REMATCH[3]}
        ip=${BASH_REMATCH[4]}

		if [[ $(sshCheck $hostname) -eq 0 ]];then
			errorOutput "$hostname IS DOWN"
			continue
		fi

		replaceInFile "$hostname" "/etc/hosts" "$HOSTS"
		exit
		
	fi
done
exit

for line in $(cat $REPPATH/conf/appservers)
do
   if [[ $line =~ $APPSERVERREGEX ]]
   then
		ip=${BASH_REMATCH[1]}
		hostname=${BASH_REMATCH[2]}
	   
		if [[ $(sshCheck $hostname) -eq 0 ]];then
			errorOutput "$hostname IS DOWN"
			continue
		fi
	
		replaceInFile "$hostname" "/etc/hosts" "$HOSTS"
   fi
done

# poolConf "$POOLCONF"

# replaceInFile "/etc/hosts" "$HOSTS"
# hbaConf "$HBACONF"
# poolConf "$POOLCONF"
# echo -e "$HBACONF"

#
# for i in "${!POOL[@]}"
# do
#   echo "key  : $i"
#   echo "value: ${POOL[$i]}"
# done