#!/bin/bash
apt-get -y -q update
apt-get -y -q install build-essential incron postgresql-9.3 postgresql-server-dev-9.3 postgresql-contrib-9.3 msmtp libxslt-dev libxml2-dev libpam-dev libedit-dev postgresql-9.3-repmgr
echo "$1" > /etc/hostname
echo "$1" >> /proc/sys/kernel/hostname
! grep -Pq "127.0.0.1[\s\t]+?$1$" /etc/hosts && echo -e "127.0.0.1\t$1" >> /etc/hosts

cd /tmp
wget https://github.com/2ndQuadrant/repmgr/archive/v2.0.2.tar.gz
tar xf v2.0.2.tar.gz

cd /tmp/repmgr-2.0.2 && make USE_PGXS=1 &&make USE_PGXS=1 install
echo "postgres:p123" | chpasswd
echo 'PATH=/usr/lib/postgresql/9.3/bin:$PATH' >> /root/.profile
echo 'PATH=/usr/lib/postgresql/9.3/bin:$PATH' >> /var/lib/postgresql/.profile
update-rc.d postgresql disable
service postgresql stop
# exit;
[[ ! -d /data ]] && mkdir /data 
[[ ! -d /root/replicads  ]] && mkdir -p /root/replicads/{pid,flags}
[[ ! -d /var/run/postgresql ]] && mkdir /var/run/postgresql
[[ ! -d /var/log/replicads ]] && mkdir /var/log/replicads && chmod 777 /var/log/replicads

chown postgres:postgres /data

sudo -u postgres /usr/lib/postgresql/9.3/bin/initdb -D /data

killall postgres

ssh-keygen -A -t rsa

echo -e "ControlMaster auto
ControlPath /tmp/%r@%h:%p
ServerAliveInterval 60" > /root/.ssh/config

service ssh restart

chown postgres:postgres /var/run/postgresql

echo "INSTALLATIONCOMPLETE"