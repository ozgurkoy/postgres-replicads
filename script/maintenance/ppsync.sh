#!/bin/bash
[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/$(basename $0)"; exit 1; }

export MAINLOG="/var/log/replicads/sync"
export DOINFOLOG=1

source def/vars.sh
source def/functions.sh

warningOutput "SYNCING WITH OTHER PPS"
for line in $(cat /root/replicads/conf/servers)
do
   if [[ $line =~ $PPREGEX ]]
   then
		host=${BASH_REMATCH[1]}
		ip=${BASH_REMATCH[3]}
		
		[[ $host == $SELFHOST ]] && continue;
		
		if [[ $(sshCheck $host) -eq 0 ]] ;then 
			errorOutput "PP ($host ) can't be reached for ppsync"
			continue
		fi
		infoOutput "Syncing with $host"
		ssh $SSHOPT root@$ip "rm $FLAGPATH/*"
		
		if [[ $1 == "script" ]];then
			rsync -azh --exclude "ppflags"  --exclude "pid" --exclude ".git" --exclude "log" -e "ssh -o StrictHostKeyChecking=no" $REPPATH/* root@$ip:$REPPATH
			rsync -azh -e "ssh -o StrictHostKeyChecking=no" $REPPATH/template/replicads root@$ip:/etc/init.d/replicads
			ssh $SSHOPT root@$ip "/etc/init.d/replicads restart"
		else
			rsync -azh --exclude "ppflags" --exclude "/def" --exclude "/node" --exclude "/script" --exclude "/template"  --exclude "pid" --exclude ".git" --exclude "log" -e "ssh -o StrictHostKeyChecking=no" $REPPATH/* root@$ip:$REPPATH
		fi
		
	fi
done