#!/bin/bash


daemonName="REPLICADS_PP"
PPFLAGPATH="/root/replicads/ppflags"
FLAGPATH="/root/replicads/flags"
pidDir="/root/replicads/pid"
pidFile="$pidDir/$daemonName.pid"

# pidFile="$daemonName.pid"
# echo $pidFile
# exit
logDir="/var/log/replicads"
# To use a dated log file.
# logFile="$logDir/$daemonName-"`date +"%Y-%m-%d"`".log"
# To use a regular log file.
logFile="$logDir/$daemonName.log"

# Log maxsize in KB
logMaxSize=1024	 # 1mb

runInterval=4 # In seconds
startTime=`date +%s`
runTime=`date +%s`
echo "DATE : $(date +%H:%M\ %d/%m/%Y)"
lastRun=0
declare -A COMMANDS
declare -A INTERVALS
declare -A NEXTEXEC

COMMANDS["status"]='source /root/replicads/script/pp.sh "checkPPStatus" >> $logFile'
COMMANDS["nodeIntCon"]='source /root/replicads/script/pp.sh "nodeIntCon" >> $logFile'
COMMANDS["checkNodes"]='source /root/replicads/script/pp.sh "checkNodes" >> $logFile'
COMMANDS["ssh"]='/root/replicads/script/pp.sh "establishSSHConnections" >> $logFile'
#must run if it's the master
COMMANDS["ppsync"]='/root/replicads/script/maintenance/ppsync.sh >> $logFile'

INTERVALS["status"]=6
INTERVALS["nodeIntCon"]=12
INTERVALS["checkNodes"]=6
INTERVALS["ssh"]=20
INTERVALS["ppsync"]=30

NEXTEXEC["status"]=$runTime
NEXTEXEC["ignite"]=$runTime
NEXTEXEC["ignite"]=$runTime
NEXTEXEC["ssh"]=$runTime
NEXTEXEC["ppsync"]=$runTime

export MASTERFAILCOUNT=0

doCommands() {
	# This is where you put all the commands for the daemon.
	# echo $PPFLAGPATH/isMaster
	if [[ ! -f $PPFLAGPATH/isMaster ]]; then
		log "We can't PRoceed because the PP is not master"
		return
	fi
	
	if [[ -f $PPFLAGPATH/killPP ]]; then
		log "We can't PRoceed because there's a kill pp flag"
		return
		# rm  $PPFLAGPATH/killPP
	fi
	
	log "
	*****
	Running commands[$daemonName]...
	*****
	"
	
	if [[ -f "$PPFLAGPATH/syncop" ]];then 
		log "SYNC OPERATION IN PROGRESS. SKIPPING."
		return
	fi

	for K in "${!COMMANDS[@]}"; 
		do 
		# echo $runTime
		intv=${INTERVALS[$K]}
		nexec=${NEXTEXEC[$K]}
		cmd=${COMMANDS[$K]}
		now=`date +%s`
		if [[ $nexec -le $runTime ]];then
			# log "$K -- $runTime -- $intv -- $nexec"
			# echo $K --- ${COMMANDS[$K]} -- ${INTERVALS[$K]} -- ${NEXTEXEC[$K]};
			eval $cmd
			NEXTEXEC[$K]=$((now+intv))
		# else
		# 	echo "SKIPPING $K $now"
		fi
	done
}

################################################################################
# Below is the skeleton functionality of the daemon.
################################################################################

myPid=`echo $$`

setupDaemon() {
	# Make sure that the directories work.
	if [ ! -d "$pidDir" ]; then
		mkdir "$pidDir"
	fi
	if [ ! -d "$logDir" ]; then
		mkdir "$logDir"
	fi
	if [ ! -f "$logFile" ]; then
		touch "$logFile"
	else
		# Check to see if we need to rotate the logs.
		size=$((`ls -l "$logFile" | cut -d " " -f 5`/1024))
		if [[ $size -gt $logMaxSize ]]; then
			mv $logFile "$logFile.old"
			touch "$logFile"
		fi
	fi
	
}

startDaemon() {
	PRV=$(ps aux|grep '[/]bin/bash script/daemon.sh start|wc -l')
	
	if [[ -f $pidFile ]]; then
		echo "A PROCESS IS ALREADY RUNNING ($pidFile - pid : $myPid)"
		exit
	fi
	
	if [[ $PRV -gt 0 ]];then 
		echo "PROCESS ALREADY RUNNING, CAN'T START AGAIN"
		echo "PROCESS ALREADY RUNNING, CAN'T START AGAIN" >> /root/uprlog
		exit
	fi
	
	# Start the daemon.
	setupDaemon # Make sure the directories are there.
	if [[ $(checkDaemon) -eq 1 ]]; then
		echo " * \033[31;5;148mError\033[39m: $daemonName is already running."
		exit 1
	fi
	echo " * Starting $daemonName with PID: $myPid."
	echo "$myPid" > "$pidFile"
	log '*** '`date +"%Y-%m-%d"`": Starting up $daemonName."
	
	#remove flag, since we are just starting
	[[ -f /root/replicads/flags/daemonop ]] && rm /root/replicads/flags/daemonop
	
	# Start the loop.
	loop
}

stopDaemon() {
	# Stop the daemon.
	if [[ $(checkDaemon) -eq 0 ]]; then
		echo " * \033[31;5;148mError\033[39m: $daemonName is not running."
		exit 1
	fi
	echo " * Stopping $daemonName"
	log '*** '`date +"%Y-%m-%d"`": $daemonName stopped."
	if [[ ! -z `cat $pidFile` ]]; then
		kill -9 `cat "$pidFile"` &> /dev/null
	fi
	[[ -f "$pidFile" ]] && rm "$pidFile"
	
	# touch $PPFLAGPATH/killPP
}

statusDaemon() {
	# Query and return whether the daemon is running.
	if [[ $(checkDaemon) -eq 1 ]]; then
		echo " * $daemonName is running."
	else
		echo " * $daemonName isn't running."
	fi
	exit 0
}

restartDaemon() {
	# Restart the daemon.
	if [[ $(checkDaemon) = 0 ]]; then
		# Can't restart it if it isn't running.
		echo "$daemonName isn't running."
		exit 1
	fi
	stopDaemon
	startDaemon
}

checkDaemon() {
	# Check to see if the daemon is running.
	# This is a different function than statusDaemon
	# so that we can use it other functions.
	if [ -z "$oldPid" ]; then
		echo 0
	elif [[ `ps aux | grep "$oldPid" | grep -v grep` > /dev/null ]]; then
		if [ -f "$pidFile" ]; then
			if [[ `cat "$pidFile"` = "$oldPid" ]]; then
				# Daemon is running.
				echo 1
				# return 1
			else
				# Daemon isn't running.
				rm "$pidFile"
				echo 0
				# return 0
			fi
		fi
	# elif [[ `ps aux | grep "$daemonName" | grep -v grep | grep -v "$myPid" | grep -v "0:00.00"` > /dev/null ]]; then
	# 	# Daemon is running but without the correct PID. Restart it.
	# 	log '*** '`date +"%Y-%m-%d"`": $daemonName running with invalid PID; restarting."
	# 	restartDaemon
	# 	return 1
	else
		# Daemon not running.
		[[ -f "$pidFile" ]] && rm "$pidFile"
		echo 0
	fi
	return 1
}

loop() {
	# This is the loop.
	now=`date +%s`

	if [ -z $last ]; then
		last=`date +%s`
	fi

	# Do everything you need the daemon to do.
	doCommands

	# Check to see how long we actually need to sleep for. If we want this to run
	# once a minute and it's taken more than a minute, then we should just run it
	# anyway.
	last=`date +%s`

	# Set the sleep interval
	log "out for "$((last-now+1));
	# if [[ ! $((now-last+runInterval+1)) -lt $runInterval ]]; then
	if [[ $((last-now+1)) -lt $runInterval ]]; then
		log "SLEEPING $((runInterval-last+now-1))"
		sleep $((runInterval-last+now-1))
	fi
	runTime=`date +%s`
	
	if [[ -f $FLAGPATH/restartDaemon ]]; then
		rm $FLAGPATH/restartDaemon
		log "RESTARTING DAEMON DUE TO FLAG"
		/etc/init.d/replicads restart
		# restartDaemon
		return
	fi
	
	# Startover
	loop
}

log() {
	# Generic log function.
	echo "$1" >> "$logFile"
}


################################################################################
# Parse the command.
################################################################################

if [ -f "$pidFile" ]; then
	oldPid=`cat "$pidFile"`
fi
# $(checkDaemon)
case "$1" in
	start)
		startDaemon
		;;
	stop)
		stopDaemon
		;;
	status)
		statusDaemon
		;;
	restart)
		restartDaemon
		;;
	*)
	echo "\033[31;5;148mError\033[39m: usage $0 { start | stop | restart | status }"
	exit 1
esac

exit 0