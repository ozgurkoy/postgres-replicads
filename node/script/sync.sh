#!/bin/bash
SELF=`hostname`
REGEX="^\[(.+?)\]\[node\]\[(.+?)\]\[(.+?)\]"

for line in $(cat /root/replicads/conf/servers)
do
   if [[ $line =~ $REGEX ]]
   then
        match=${BASH_REMATCH[1]}
        if [[ $SELF != $match ]]
        then
            echo "Updating files(conf,sh) on $match"
			rsync -avzh -e "ssh" --backup --suffix=-`date +%F-%T` /root/replicads/{conf,sh}  root@$match:/root/replicads/
        fi
   fi
done