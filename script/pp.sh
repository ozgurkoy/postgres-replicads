#!/bin/bash
[[ -d def ]] || { echo >&2 "Run the script from the replicads folder like script/$(basename $0)"; exit 1; }
export MAINLOG="/var/log/replicads/pp"

source def/vars.sh
source def/functions.sh

findMasterPP(){
	REGEX="^\[(.+?)\]\[pp\]\[(.+?)\]\[(.+?)\]"

	for line in $(cat $REPPATH/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        weight=${BASH_REMATCH[2]}
	        ip=${BASH_REMATCH[3]}

			isMaster=$(ssh $SSHOPT -q root@$ip "[[ -f \"$PPFLAGPATH/isMaster\" ]] && echo \"good\"")

			if [[ $isMaster == "good" ]];then
				echo $host
				return
			fi
	   fi
	done
	echo "none"
}

amIMaster(){
	( [[ -f "$PPFLAGPATH/isMaster" ]] && echo 1 ) || echo 0
}

loadServers() {
	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        type=${BASH_REMATCH[2]}

			ALLSERVERS+=($host)

			if [[ $(portcheck ${host} 5432) -eq 0 ]] ;then
				errorOutput "${host} IS DOWN"
				continue
			fi
			
			if [[ $type == "node" ]];then
				SERVERS+=($host)
			elif [[ $type == "pp" ]];then
				POOLS+=($host)
			fi
			
			GOODSERVERS+=($host)
			
	   fi
	done
	# echo ${SERVERS[@]}
}

restartPP() {
	[[ $(checkOp) -eq 1 ]] && errorOutput "ONGOING OPERATION, WON'T EXECUTE" && return	
	
	eff="notgood"
	c=1
	#some bug on the while comparator
    while [ "${eff}"=="notgood" ];do
		# echo $eff
		warningOutput "Trying for the $c time"
		service pgpool2 restart
		sleep 2
	    eff=$(ppIsUp)
        c=$(($c + 1))
		if [[ "${eff}"=="good" ]];then
			break;
		elif [[ $c -gt 2 ]];then
			errorOutput "tried enough,wont start"
			break
		fi
    done
	clearOp
}

checkPPStatus() {
	ppStatus=$(ppIsUp)
	portStatus=$(portcheck "$SELFHOST" 5432)
	connStatus=`expr "$(nodeCount)" : '.*Error'`
	if [[ $ppStatus != "good" || $portStatus -ne 1 || $connStatus -gt 0 ]];then
		errorOutput "Something is up, restarting"
		killall pgpool
		restartPP
	else
		infoOutput "PP is good"
	fi
}

ppIsUp() {
    pstatus="$(service pgpool2 status)"
    eff=$(echo "$pstatus"|sed 's/.*\(is running\).*/good/')
    if [[ "$eff" != "good" ]]
    then
		eff="notgood"
    fi
    echo $eff
}

restartServers() {
	[[ $(checkOp) -eq 1 ]] && errorOutput "ONGOING OPERATION, WON'T EXECUTE" && return	
	
	warningOutput "RESTARTING SERVERS"

	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $NODEREGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        type=${BASH_REMATCH[2]}

		   	warningOutput "RESTART SERVER  : $host"
			[[ $(portcheck $host 5432) -eq 0 ]] && errorOutput "SERVER IS DOWN($host)" && continue
			ssh $SSHOPT root@$host "$DAEMONSCRIPT restart"
		fi
	done
	
	sleep 5
	clearOp
}

ppsync() {
	warningOutput "SYNCING EVERYTHING WITH OTHER PPS"

	for line in $(cat /root/replicads/conf/servers)
	do
 	   if [[ $line =~ $PPREGEX ]]
 	   then
 	        host=${BASH_REMATCH[1]}
 	        ip=${BASH_REMATCH[3]}
			
			[[ $host == $SELFHOST ]] && continue;
			
			if [[ $(sshCheck $host) -eq 0 ]] ;then 
				errorOutput "PP ($host ) can't be reached for ppsync"
				continue
			fi
			
			rsync -azh  --exclude "pid" --exclude ".git" --exclude "log" -e "ssh -o StrictHostKeyChecking=no" $REPPATH/* root@$ip:$REPPATH
		fi
	done
}

nodes() {
	count=$(nodeCount)
	warningOutput "Total nodes : "$count
	if [[ -z $1 || $1 == "all" ]] 
	then
		count=$(nodeCount)
		for ((i=0;i<$count;i++)); 
			do nodeInfo $i
		done;
	else
		nodeInfo $1;
	fi
}

changeMaster() {
	if [[ -z $2 ]];then
		[[ $(checkOp) -eq 1 ]] && errorOutput "ONGOING OPERATION, WON'T EXECUTE" && return	
	fi
		
	if [[ -z $1 || $(nodeExists $1 "connectable") -eq 0 ]];then 
		errorOutput "MUST CHOOSE A VALID AND CONNECTABLE NODE";
		return 1
	fi
	
	if [[ $(sshCheck $1) -eq 0 ]] ;then 
		errorOutput "CAN'T REACH NEW MASTER CANDIDATE($1)";
		return 1
	fi
		
	currentM=$(findCurrentMaster)
	if [[ -z $2 && "$currentM" == "none" ]];then
		errorOutput "NO ACTIVE MASTER FOUND"
		clearOp
		return 1
	elif [[ -z $2 && "$currentM" == "$1" ]];then
		errorOutput "THE MASTER IS THE SAME $1"
		clearOp
		return 1
	fi
	
	newMasterStatus=$(getNodeStatus "$1")
	operation=`expr "$newMasterStatus" : '^\([^~]*\)'`
	detail=`expr "$newMasterStatus" : '^.*~\(.*\)'`
	
	if [[ $operation != "good" ]];then
		errorOutput "New Master($1) is currently busy. Current status : $newMasterStatus"
		return 1
	fi
	
	#put the flag the old one is dead now.
	if [[ ! -z $2 ]];then
		currentM=$2
	fi
	
	if [[ $(sshCheck $currentM) -eq 0 ]] ;then 
		errorOutput "CAN'T REACH MASTER($currentM)"
		masterxlog=0
		# clearOp
		# exit
	else
		[[ $DEBUG -eq 1 ]] && echo "checking xlog master"
		masterxlog=$(nodeXlog "$currentM")
	fi

	[[ $DEBUG -eq 1 ]] && echo "checking xlog $1"
	nodexlog=$(nodeXlog "$1")
	[[ $DEBUG -eq 1 ]] && echo "XLOG ANSWER $nodexlog" >> /var/log/replicads/REPLICADS_PP.log
	
	[[ $DEBUG -eq 1 ]] && echo "comparing node xlog"
	diff=$((masterxlog-nodexlog))
	[[ $DEBUG -eq 1 ]] && echo "compared node xlog"
	
	if [[ $diff -gt $MAXBYTEDIF ]];then
		errorOutput "$node is too behind of master ($diff bytes). Can't change it"		
	else
		if [[ ! -f "$FLAGPATH/kill-$currentM" ]];then
			warningOutput "Killing current master >> $currentM <<"
			putFlag "$currentM" "clone" "$1"
			killServer $currentM
			sleep 3
		else
			warningOutput "Current master is already marked as killed."
		fi

		warningOutput "Promoting $1"

		promote "$1" 
	fi
	# exit
	# sleep 7
	[[ -z $2 ]] && clearOp
	checkMaster
}

checkOp(){
	if [[ -f "$FLAGPATH/daemonop" ]];then 
		oldd=$(cat $FLAGPATH/daemonop)
		curd=$(date '+%s')
		if [[ $(($curd-$oldd)) -le 300 ]];then
			echo 1
			return 
		fi
	fi
	
	date '+%s' >  "$FLAGPATH/daemonop"
}

clearOp(){
	[[ -f "$FLAGPATH/daemonop" ]] && rm "$FLAGPATH/daemonop"
}

nodeInfo() {
	pcp_node_info 1 localhost 9898 admin "$POOLPASS" $1
}

dbStats() {
	for i in "${!SERVERS[@]}"
	do
	   	warningOutput "SERVER  : ${SERVERS[$i]}"
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && echo "SERVER IS DOWN(${SERVERS[$i]})" && continue
		ssh $SSHOPT root@${SERVERS[$i]} "$NODESCRIPT status"
	done
}

nodeCount() {
	echo `pcp_node_count 1 localhost 9898 admin "$POOLPASS"`
}

recoveryQuery() {
	[[ $(portcheck $1 5432) -eq 0 ]] && echo "PSQL CONNECTION ERROR" && return
	
	eff=`psql -t --username=repmgr --dbname=repmgr --host $1 -w -c "SELECT pg_is_in_recovery()"|& grep -P -i ^.+$`
	eff=$(echo $eff| sed -e 's/^ *//' -e 's/ *$//')
	
	echo $eff
}

findNodeId() {
	nodeCount=$(nodeCount)
	inc=0

	for ((i=0;i<$nodeCount;i++)); 
		do 	
		eff=$(pcp_node_info 1 localhost 9898 admin "$POOLPASS" $i)
		
		[[ $eff =~  ^([a-zA-Z0-9-]+)[[:space:]]([0-9-]+)[[:space:]]([0-9-]+)[[:space:]] ]]
		# echo $BASH_REMATCH
		status=${BASH_REMATCH[3]}
		node=${BASH_REMATCH[1]}
		if [[ $node == $1 ]];then
			echo $i
			return
		fi
	done;
	echo -1
}

detachNode() {
	nodeId=$(findNodeId "$1")
	[[ $nodeId -lt 0 ]] && errorOutput "Node not found" && return
	
	putAttachFlag $1
	warningOutput "DETACHING $1"
	detach "$1"
	# /usr/sbin/pcp_detach_node 0 $SELFHOST 9898 admin "$POOLPASS" $nodeId
}

detach() {
	nodeId=$(findNodeId "$1")
	warningOutput "DETACHING $1"
	/usr/sbin/pcp_detach_node 0 $SELFHOST 9898 admin "$POOLPASS" $nodeId
}

putFlag() {
	fdetail="$3"
	currentStatus=$(getNodeStatus "$1")
	operation=`expr "$currentStatus" : '^\([^~]*\)'`
	detail=`expr "$currentStatus" : '^.*~\(.*\)'`
	# infoOutput "$1-$operation"
	infoOutput "PUTFLAG : CURRENT STATUS : $operation $detail . WANTED : $2 $fdetail"
	if [[ "$operation" == "$2" && "$detail" == "$fdetail" ]];then
		warningOutput "$1 is already in state : $2|$fdetail"
		return 1
	fi
	
	warningOutput "LEFT $2~$fdetail FLAG FOR $1" 
	echo "$2~$fdetail">"$FLAGPATH/$1"
}

putAttachFlag() {
	# ssh $SSHOPT root@$CONFIGHOST "touch $FLAGPATH/noattach-$1"
	touch "$FLAGPATH/noattach-$1"
	warningOutput "LEFT NOATTACH FLAG FOR $1"
}

clearFlags() {
	S=""
	for i in "${!SERVERS[@]}"
	do
		if [[ ! -z $1 && ${SERVERS[$i]} != $1  ]];then
			continue;
		fi

		# ssh $SSHOPT root@$CONFIGHOST "[[ -f $FLAGPATH/${SERVERS[$i]} ]] && rm $FLAGPATH/${SERVERS[$i]}"
		[[ -f "$FLAGPATH/${SERVERS[$i]}" ]] && rm "$FLAGPATH/${SERVERS[$i]}"
		
	done
	infoOutput "CLEARED FLAGS"
}

clearAttFlags() {
	S=""
	for i in "${!SERVERS[@]}"
	do
		if [[ ! -z $1 && ${SERVERS[$i]} != $1  ]];then
			continue;
		fi

		# ssh $SSHOPT root@$CONFIGHOST "[[ -f $FLAGPATH/noattach-${SERVERS[$i]} ]] && rm $FLAGPATH/noattach-${SERVERS[$i]}"
		[[ -f "$FLAGPATH/noattach-${SERVERS[$i]}" ]] && rm "$FLAGPATH/noattach-${SERVERS[$i]}"
		
	done
	infoOutput "CLEARED ATT FLAGS"
}

attachNode() {
	nodeId=$(findNodeId "$1")
	[[ $nodeId -lt 0 ]] && errorOutput "Node not found" && return
	
	infoOutput "ATTACHING $1" "log"
	if [[ -f "$FLAGPATH/noattach-$1" ]];then
		# errorOutput "Can't attach node yet, NOATTACH FLAG IS PRESENT"
		# exit
		rm "$FLAGPATH/noattach-$1"
	fi
	
	/usr/sbin/pcp_attach_node 0 $SELFHOST 9898 admin "$POOLPASS" $nodeId
}

findCurrentMaster() {
	foundmaster="none"
	for i in "${!SERVERS[@]}"
	do
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && continue
		
		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "f" ]]
		then
			foundmaster=${SERVERS[$i]}
		fi
	done
	echo $foundmaster
}

loggedMaster() {
	echo $(tail -1 $CONFPATH/masters | head -1 | sed 's/\(^.*\)|.*/\1/')
}

checkMaster() {
	# checkOp

	loggedM=$(loggedMaster)
	# echo $loggedM
	currentM=$(findCurrentMaster)
	# exit
	infoOutput "CURRENT MASTER $currentM"
	infoOutput "LOGGED MASTER $loggedM"
	if [[ $currentM == "none" ]];then
		errorOutput "CHECKMASTER : NO MASTER FOUND"
	elif [[ $loggedM != $currentM && ${#currentM} -gt 0 ]];then
		warningOutput "CHECKMASTER : MASTER CHANGE from $loggedM to $currentM"
		echo $currentM"|"$(date --iso-8601=seconds --utc)>>"$CONFPATH/masters"
		# checkNodes
		followSlaves "$currentM"
	elif [[ $loggedM == $currentM ]];then
		infoOutput "CHECKMASTER : MASTER IS THE SAME -> $loggedM"
	fi

	clearOp
}

followSlaves() {
	#we are forcing the slaves to clone and start. happens on master change.
	currentMaster=$1
	warningOutput "FOLLOWING SLAVES"
	for i in "${!SERVERS[@]}"
	do
		if [[ -f "$FLAGPATH/kill-${SERVERS[$i]}" ]];then
			continue
		fi
		
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && continue
		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "t" ]]
		then
			warningOutput "FOLLOWING SLAVE ${SERVERS[$i]}"
			# putFlag "${SERVERS[$i]}" "clone" "$currentMaster"
			putFlag "${SERVERS[$i]}" "followMaster" "$currentMaster"
			
			# ssh $SSHOPT root@${SERVERS[$i]} "$NODESCRIPT clonestart"
		fi
	done
}

portcheck() {
	if ! nc -w 5 -z $1 $2 2>/dev/null
	then
		echo "0"
	else
		echo "1"
		# exit
	fi
}

nodeIntCon() {
	infoOutput "TESTING INTCON"
	loggedM=$(loggedMaster)
	currentM=$(findCurrentMaster)
	# echo $loggedM
	
	if [[ $loggedM != $currentM ]];then
		errorOutput "LOGGED AND ACTIVE MASTER ARE DIFFERENT. LOGGED: $loggedM CURRENT: $currentM"
		return 1
		# exit
	fi
	
	if [[ $currentM == "none" ]];then
		errorOutput "CURRENT MASTER CAN'T BE FOUND"
		return 1
	fi
	
	if [[ $(sshCheck $loggedM) -eq 0 ]] ;then
		errorOutput "LOGGED MASTER($loggedM) CAN'T BE CONNECTED"
		return 1
	fi
	
	status=`ssh $SSHOPT root@$loggedM "$NODESCRIPT checkSlaveConn" |& grep -P -i ^.+$`
	badServers=($(echo $status | grep -oP "NOTGOOD:\[\K([^\]])+"));
	anybad=0
	
	clearAttFlags #better way is to check for active flags and remove them. 
	
	if [[ ${#badServers} -gt 0 ]]; then
		anybad=1
		errorOutput "OH MAN BAD SERVERS";
    	for i in "${!badServers[@]}"
    	do
			detachNode "${badServers[$i]}"
			putAttachFlag "${badServers[$i]}"
		done
	else
		infoOutput "Intcon is good"
	fi
	# checkNodes #daemon job now
}

igniteNode() {
	CURD=$(date '+%s')
	
	if [[ ! -f "$FLAGPATH/lastIgnite-$1" ]];then
		echo $((CURD+MINIGNITERETY)) > "$FLAGPATH/lastIgnite-$1"
	fi
	
	OLDD=$(cat "$FLAGPATH/lastIgnite-$1")
	DIFF=$((OLDD-CURD))
	
	echo "$CURD > $OLDD "
	if [[ $OLDD -gt $CURD ]];then
		errorOutput "WE CAN'T IGNITE THE NODE YET AGAIN, WILL TRY AGAIN IN $DIFF SECONDS"
		return
	fi
	
	echo $((CURD+MINIGNITERETY)) > "$FLAGPATH/lastIgnite-$1"
		
	warningOutput "IGNITING $1"
	# $(/usr/bin/ssh $SSHOPT -l root "$1" "$NODESCRIPT ignite")
	/usr/bin/ssh $SSHOPT -l root "$1" "$DAEMONSCRIPT restart"
}

nodeDaemonRunning() {
	if [[ $(sshCheck $1) -eq 0 ]] ;then
		echo 0
		return 0
	fi
	
	serverDaemonStatus=`ssh $SSHOPT root@$1 "$DAEMONSCRIPT status" |& grep -P -i ^.+$`
	# echo $serverDaemonStatus
	
	kgood=`expr "$serverDaemonStatus" : '.*is running'`
	if [[ $kgood -gt 0 ]]; then
		echo 1
	else
		echo 0
	fi
}

findYoungest() {
	if [[ ! -z $1 ]];then
		warningOutput "TRYING TO FIND THE YOUNGEST SLAVE"
	fi
	biggest=0
	latestNode="none"
	for i in "${!SERVERS[@]}"
	do
		if [[ -f "$FLAGPATH/kill-${SERVERS[$i]}" || $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]];then
			if [[ ! -z $1 ]];then
				warningOutput "SKIPPING ${SERVERS[$i]} on Youngest search"
			fi
			
			continue
		fi
		
		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "t" ]]
		then
			if [[ ! -z $1 ]];then
				warningOutput "CHECKING SLAVE ${SERVERS[$i]}"
			fi
			# psql -t --username=repmgr --dbname=repmgr --host ${SERVERS[$i]} -w -c "select now() - pg_last_xact_replay_timestamp() AS replication_delay;"
			#receive or replay below?
			eff=`psql -t --username=repmgr --dbname=repmgr --host ${SERVERS[$i]} -w -c "SELECT 
        		pg_xlog_location_diff( 
                (CASE 
                        WHEN (pg_is_in_recovery()) THEN 
                                pg_last_xlog_receive_location() 
                        ELSE 
                                pg_current_xlog_location() 
                END), 
                '000/00000000' 
        		) AS total_wal_offset "|& grep -P -i ^.+$`
			if [[ ! -z $1 ]];then
				echo $eff
			fi
			if [[ $eff -gt $biggest ]];then
				biggest=$eff
				latestNode=${SERVERS[$i]}
			fi
		fi
	done
	# echo $biggest
	echo $latestNode
}

promote (){
	putFlag "$1" "promote"
	# /usr/bin/ssh $SSHOPT -l root "$1" "$NODESCRIPT promote"
}

xlogParse (){
	d=$1
	k=(${d//\// })
	left=$((16#${k[0]}))
	right=$((16#${k[1]}))
	# echo $left
	# echo $right
	# /usr/bin/ssh $SSHOPT -l root "$1" "$NODESCRIPT promote"
}

nodeXlog (){
	if [[ $(sshCheck $1) -eq 0 ]] ;then
		# echo "XLOG CANT CONNECT TO $1 22" >> /var/log/replicads/REPLICADS_PP.log
		echo 0
	else
		# echo "XLOG CONNECTED TO $1" >> /var/log/replicads/REPLICADS_PP.log
		e=$(ssh $SSHOPT root@$1 "$NODESCRIPT xlogLocation")
		# echo "XLOG ANSWER $e" >> /var/log/replicads/REPLICADS_PP.log
		if [[ $(isNumeric "$e") -eq 0 ]];then
			e=0
		fi
		echo $e
	fi
}

testNodeConnections(){
	MAXCOUNT=0
	CONNECTABLE=0
	
	for line in $(cat /root/replicads/conf/servers)
	do
		if [[ $line =~ $NODEREGEX ]];then
			host=${BASH_REMATCH[1]}

			[[ -f "$FLAGPATH/kill-$host" ]] && continue
			
			# [[ $(sshCheck $host) -eq 0 ]] && continue
			
			# echo "GOOD"
			MAXCOUNT=$((MAXCOUNT+1))
			[[ $(sshCheck $host) -eq 1 ]] && CONNECTABLE=$((CONNECTABLE+1));
		fi
	done
	# echo "MAX:$MAXCOUNT"
	# echo "CON:$CONNECTABLE"

	echo $(bc -l<<<"($CONNECTABLE/$MAXCOUNT)>$MINCONNECTRATIO")
}

killServer() {
	if [[ $(sshCheck $1) -eq 0 ]] ;then
		warningOutput "Can't reach $1 by ssh, trying from other hosts."
		# return
		for i in "${!SERVERS[@]}"
		do
			tyy="${SERVERS[$i]}"
			if [[ $tyy != $1 ]];then
				if [[ $(sshCheck $tyy) -eq 0 ]] ;then
					continue
				fi
				warningOutput "TRYING OVER $tyy"
				stopAction=`ssh $SSHOPT root@$tyy "$NODESCRIPT remoteStop $1" |& grep -P -i ^.+$`
				# echo $stopAction
				kgood=`expr "$stopAction" : '.*server stopped'`
				# sleep 4
				# checkMaster
				if [[ $kgood -gt 0 ]]; then
					warningOutput "SERVER STOPPED ( $1 )"
					break
				fi
			fi
		done
	else
		stopAction=`ssh $SSHOPT root@$1 "$NODESCRIPT faststop" |& grep -P -i ^.+$`
		echo $stopAction
	fi
}

checkSyncOp(){
	if [[ -f "$PPFLAGPATH/syncop" ]];then
		oldd=$(cat $PPFLAGPATH/syncop)
		curd=$(date '+%s')

		if [[ $(isNumeric "$oldd") -eq 0 ]];then
			oldd=0
		fi

		if [[ $(($curd-$oldd)) -le $SYNCOPTIMEOUT ]];then
			echo 1
			return
		fi
	fi
	rm "$PPFLAGPATH/syncop"
}

checkNodes() {
	infoOutput "RUNNING CHECKNODES"
	# checkMaster
	
	if [[ -f "$PPFLAGPATH/restartServers" ]];then
		warningOutput "RESTART SERVERS FLAG FOUND RESTARTING."
		restartServers
		rm "$PPFLAGPATH/restartServers"
		return 1
	fi

	if [[ -f "$PPFLAGPATH/restartPP" ]];then
		warningOutput "RESTART PP FLAG FOUND RESTARTING."
		restartPP
		rm "$PPFLAGPATH/restartPP"
		clearOp
		return 1
	fi

	[[ $(checkSyncOp) -eq 1 ]] && errorOutput "SKIPPING IGNITE - SYNCOP" && return
	[[ $(checkOp) -eq 1 ]] && errorOutput "ONGOING OPERATION, WON'T EXECUTE" && return	
	
	inc=0
	nodeCount=$(nodeCount)
	loggedM=$(loggedMaster)
	activeServerCount=${#SERVERS[@]}
	if [[ $activeServerCount -eq 0 ]];then
		errorOutput "ALL SERVERS ARE DOWN, OR CAN'T REACH" "urgent"
	fi
	[[ $DEBUG -eq 1 ]] && echo "checking current master"
	currentM=$(findCurrentMaster)
		
	#if no master AND there are some servers active 
	if [[ $currentM == "none"  && $activeServerCount -gt 0 ]];then
		if [[ $MASTERFAILCOUNT -lt $MAXMASTERFAILCOUNT ]];then
			MASTERFAILCOUNT=$((MASTERFAILCOUNT+1))
			errorOutput "NO MASTERS FOUND - TRY : $MASTERFAILCOUNT"
			errorOutput "SKIPPING MASTER COUNT FOR THE $MASTERFAILCOUNT."
			sleep $MASTERRETRYSLEEP 
			warningOutput "SLEEPING FOR $MASTERRETRYSLEEP"
			clearOp
			return 1
		else
			errorOutput "NO MASTERS FOUND - AFTER $MAXMASTERFAILCOUNT TRIES" "urgent"
			MASTERFAILCOUNT=0
		fi
		
		young=$(findYoungest)
		#if there's a kill flag for the master don't bother now.
		if [[ ! -f "$FLAGPATH/kill-$loggedM" ]];then
			if [[ "$young" != "none" ]];then
				putFlag "$loggedM" "clone" "$young"
			fi
			killServer "$loggedM"
		fi
		
		
		if [[ "$young" != "none" ]];then
			newMasterStatus=$(getNodeStatus "$young")
			operation=`expr "$newMasterStatus" : '^\([^~]*\)'`
			detail=`expr "$newMasterStatus" : '^.*~\(.*\)'`
	
			if [[ $operation != "good" ]];then
				errorOutput "Choosen new Master($young) is currently busy. Current status : $newMasterStatus. Skipping for now."
				clearOp
				return 1
			fi
			
			
			warningOutput "Promoting latest slave $young"
			clearFlags "$young"
			changeMaster "$young" "$loggedM"

			sleep 6
		fi
	fi
	
	if [[ $(sshCheck $currentM ) -eq 0 ]] ;then 
		errorOutput "CAN'T REACH MASTER"
		masterxlog=0
	else
		[[ $DEBUG -eq 1 ]] && echo "checking xlog master"
		masterxlog=$(nodeXlog "$currentM")
	fi
	
	[[ $DEBUG -eq 1 ]] && echo "checking multi master"
	mm=$(multiMasterCheck)

	if [[ $mm == 1 ]];then
		inc=1
		errorOutput "ERROR : MULTIPLE MASTERS FOUND, KILLING THE OLDER MASTER"
		killOlderMaster
		clearOp
		#return now
		return 0
		# stopAction=`ssh $SSHOPT root@$node "$NODESCRIPT faststop" |& grep -P -i ^.+$`
	fi
	
	for ((i=0;i<$nodeCount;i++));
		do
		eff=$(pcp_node_info 1 localhost 9898 admin "$POOLPASS" $i)
		# echo $eff
		[[ $eff =~  ^([a-zA-Z0-9-]+)[[:space:]]([0-9-]+)[[:space:]]([0-9-]+)[[:space:]] ]]
		# echo $BASH_REMATCH
		status=${BASH_REMATCH[3]}
		node=${BASH_REMATCH[1]}
		infoOutput "checking NODE $node"

		if [[ $(sshCheck $node) -eq 0 ]] ;then 
			inc=1
			errorOutput "SSH IS DOWN ,SKIPPING NODE $node"
			continue
		fi
		
		if [[ $(nodeDaemonRunning $node) -eq 0 ]] ;then
			inc=1
			errorOutput "SERVER IS DOWN($node), TRYING TO IGNITE" 
			igniteNode $node
			# sleep 3
			continue
		fi

		
		if [[ -f "$FLAGPATH/kill-$node" ]];then
			warningOutput "Kill flag found, skipping ($node)"
			inc=1
			
			continue
		elif [[ ! -f "$FLAGPATH/activenode-$node" ]];then
			warningOutput "No active flag found, skipping ($node)"
			inc=1
			
			continue
		elif [[ -f "$FLAGPATH/noattach-$node" ]];then
			warningOutput "No attach flag found, skipping ($node)"
			inc=1

			continue
		fi
		
		nodeTaskStatus=$(getNodeStatus "$node")
		operation=`expr "$nodeTaskStatus" : '^\([^~]*\)'`
		
		# continue
		[[ $DEBUG -eq 1 ]] && echo "checking xlog $node"
		nodexlog=$(nodeXlog "$node")
		[[ $DEBUG -eq 1 ]] && echo "XLOG ANSWER $nodexlog" >> /var/log/replicads/REPLICADS_PP.log
		
		# echo "$masterxlog $nodexlog"
		[[ $DEBUG -eq 1 ]] && echo "comparing node xlog"
		diff=$((masterxlog-nodexlog))
		[[ $DEBUG -eq 1 ]] && echo "compared node xlog"
		if [[ $diff -gt $MAXBYTEDIF ]];then
			errorOutput "$operation IS GOING ON FOR $node<<<<<<<"
			
			if [[ $operation == "clone" || $operation == "start" || $operation == "follow" ]];then
				warningOutput "$operation IS GOING ON FOR $node. JUST DETACHING FOR NOW."
				detach "$node"
			else
				warningOutput "$node is too behind of master ($diff bytes). Detaching"
				if [[ $diff -gt $CLONEBYTEDIF ]];then
					warningOutput "$node is too behind of master ($diff bytes). Forcing clone"
					putFlag "$node" "clone" "$currentM"
				else
					putFlag "$node" "clone" "$currentM" # anything to force?
				fi
			
				detach "$node"
			
				inc=1
			fi
			continue
		fi
		[[ $DEBUG -eq 1 ]] && echo "RECOVERY QUERY NODE"
		efg=$(recoveryQuery "$node")

		if [[ $status -eq 3 ]]; then
			inc=1
			warningOutput "SERVER IS DETACHED($node)"
			
			if [[ $efg == "t" || $efg == "f" ]];then
				warningOutput "BUT IT CAN COME BACK"
				detachNode "$node"
				attachNode "$node"
				infoOutput "DET-ATT complete" "log"
			else
				inc=1
				errorOutput "IT'S DOWN OR NOT CONNECTIBLE SO CAN'T BE REVIVED ( $efg ), TRYING TO IGNITE"
				igniteNode $node
			fi
		else
			infoOutput "SERVER IS GOOD($node)"
			#if it's attached and came this far , we can flag it.
			nodeStatusUpdate "$node" "good"
		fi
			
	done;
	clearOp
	
	checkMaster

	if [[ $inc -eq 0 ]];then
		infoOutput "ALL NODES ARE GOOD"
	fi
}

multiMasterCheck() {
	cmaster=""
	for i in "${!SERVERS[@]}"
	do
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && continue
		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "f" ]];then
			if [[ $cmaster == "" ]];then
				cmaster=${SERVERS[$i]}
			elif [[ $cmaster != ${SERVERS[$i]} ]];then
				echo 1
				return 0;
			fi
			
		fi
	done
	# echo $cmaster
	echo 0
}

establishSSHConnections() {
	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        type=${BASH_REMATCH[2]}
			
			sshCon=$(ps aux | grep -P "ssh .+? [r]oot@$host$" | grep -Po "root\s+?\K([\d]+)")

			if [[ $(echo "$sshCon" | wc -l) -gt 1 ]];then
				warningOutput "Problem with SSH connections, killing them all"
				pkill -f "ssh -N -T"
			fi
			
			[[ $(sshCheck $host) -eq 0 ]] && errorOutput "$host is not connectable" && continue
			
			if [[ ! $sshCon -gt 0 ]];then
				warningOutput "Connecting to $host for ssh bridge"
				nohup ssh -N $SSHOPT  -o ServerAliveInterval=60 root@$host 2> /dev/null 1>&2 &
			else
				infoOutput "$host is connected to ssh"
			fi
		
	   fi
	done
}

killOlderMaster() {
	warningOutput "KILLING OLDER MASTER"
	nmaster=""
	omaster=0
	ACTED=0
	MASTERSFOUND=()
	for i in "${!SERVERS[@]}"
	do
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && continue
		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "f" ]];then
			n0=$(nodeXlog "${SERVERS[$i]}")
			# echo "XLOG : ${SERVERS[$i]} - $n0"
			MASTERSFOUND+=(${SERVERS[$i]})
			
			if [[ $omaster -eq 0 ]];then
				nmaster=${SERVERS[$i]}
				omaster=$n0
			elif [[ $omaster -ge $n0 ]];then
				#this one is older than the previous
				warningOutput "KILLING-1 ${SERVERS[$i]}"
				killServer "${SERVERS[$i]}"
				putFlag "${SERVERS[$i]}" "clone" "$nmaster"
				ACTED=1
			elif [[ $n0 -ge $omaster ]];then
				#previous one is older
				warningOutput "KILLING-2 $nmaster"
				killServer "$nmaster"
				putFlag "$nmaster" "clone" "${SERVERS[$i]}"
				nmaster=${SERVERS[$i]}
				omaster=$n0
				ACTED=1
			fi
		fi
	done

	if [[ $ACTED -eq 0 ]];then
		warningOutput "XLOG ANALYZING TO KILL THE MASTER WAS UNSUCCESFUL. KILLING ALL BUT THE LASTEST LOGGED"
		loggedM=$(loggedMaster)

		for i in "${!MASTERSFOUND[@]}"
		do
			[[ ${MASTERSFOUND[$i]} == $loggedM ]] && continue;
			warningOutput "KILLING SERVER ${MASTERSFOUND[$i]}"
			killServer "${MASTERSFOUND[$i]}"
			putFlag "${MASTERSFOUND[$i]}" "clone" "$loggedM"
		done
	fi
}

nodeExists() {
	exists=0
	for line in $(cat $REPPATH/conf/servers)
	do
		if [[ $line =~ $NODEREGEX ]];then
			host=${BASH_REMATCH[1]}
			if [[ "$host" == "$1" ]];then
				if [[ $2 == "connectable" && $(sshCheck $1) -eq 1 ]];then
					exists=1
				else
					exists=1
				fi
			fi
		fi
	done
	echo $exists
}

nodeStatusUpdate() {
	node=$1
	task=$2
	echo "$task">"$FLAGPATH/nodeStatus-$node"
}

getNodeStatus(){
	status="good"
	[[ -f "$FLAGPATH/nodeStatus-$node" ]] && status=$(cat "$FLAGPATH/nodeStatus-$node")
	echo $status
}

kill(){
	warningOutput "KILLING $1"
	date '+%s' >  "$FLAGPATH/kill-$1"
	echo "kill" > "$FLAGPATH/$1"
}

unkill(){
	if [[ -f "$FLAGPATH/kill-$1" ]];then
		killDate=$(cat "$FLAGPATH/kill-$1")
		curd=$(date '+%s')
		if [[ $(($curd-$killDate)) -le $MINUNKILLTIME ]];then
			warningOutput "CAN'T UNKILL YET. MUST WAIT FOR AT LEAST $MINUNKILLTIME SECS"
			return 1
		fi
		
		loggedM=$(loggedMaster)
		if [[ "$loggedM" != "$1" ]];then
			putFlag "$1" "clone" "$loggedM"# ????
		fi
		
		rm "$FLAGPATH/kill-$1"
	fi
}

unflag(){
	[[ -f "$FLAGPATH/$1" ]] && rm "$FLAGPATH/$1"
}

unflagAttach(){
	[[ -f "$FLAGPATH/noattach-$1" ]] && rm $FLAGPATH"/noattach-"$1
}

#DEPRECATED
followMaster() {
	warningOutput "FOLLOWING MASTER FOR $1"
	ssh $SSHOPT root@$1 "$NODESCRIPT followMaster"
	# putFlag "$1" "followMaster" "$2"
	
}

#DEPRECATED
sync1() {
	warningOutput "Syncing files"
	/root/replicads/script/sync.sh	
	
	for i in "${!GOODSERVERS[@]}"
	do
		if [[ $(portcheck ${GOODSERVERS[$i]} 5432) -eq 0 ]];then
			errorOutput "${GOODSERVERS[$i]} IS DOWN"
			continue
		fi
		warningOutput "COPY ING ROOT KEY TO ${GOODSERVERS[$i]}"
		ssh-copy-id root@${GOODSERVERS[$i]}
		warningOutput "COPY ING POSTGRES KEY TO ${GOODSERVERS[$i]}"
		ssh-copy-id postgres@${GOODSERVERS[$i]}
	done
	
	for i in "${!GOODSERVERS[@]}"
	do
		warningOutput "SYNCING SSH AND PGPASS ${GOODSERVERS[$i]}"
		scp -r /root/{.ssh,.pgpass} root@${GOODSERVERS[$i]}:/root/
		scp -r /root/{.ssh,.pgpass} root@${GOODSERVERS[$i]}:/var/lib/postgresql/
		ssh $SSHOPT root@${GOODSERVERS[$i]} "chown -R postgres:postgres /var/lib/postgresql/{.ssh,.pgpass}"
	done	
}

#DEPRECATED
checkServers() {
	for i in "${!SERVERS[@]}"
	do
		eff=$(recoveryQuery "${SERVERS[$i]}")
		kerror=`expr "$eff" : '.*ERROR'`
		if [[ $kerror -gt 0 ]]; then
			echo not good
		else
			echo good	
		fi
		
	done
}

if [[ $1 != "checkPPStatus" && $1 != "amIMaster" && $1 != "findMasterPP" && $1 != "nodeDaemonRunning" && $1 != "testNodeConnections" && $1 != "establishSSHConnections" ]];then
	loadServers
fi



# exit
case "$1" in
  kill)
  	if [[ -z $2 || $(nodeExists $2) -eq 0 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    kill "$2"
    ;;
  unkill)
  	if [[ -z $2 || $(nodeExists $2) -eq 0 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    unkill "$2"
    ;;
  clearflag)
  	if [[ -z $2 || $(nodeExists $2) -eq 0 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    unflag "$2"
    ;;
  clearAttachFlag)
  	if [[ -z $2 || $(nodeExists $2) -eq 0 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    unflagAttach "$2"
    ;;
  checkMaster)
    checkMaster
    ;;
  checkNodes)
    checkNodes
    ;;
  loggedMaster)
    loggedMaster
    ;;
  nodes)
    nodes $2
    ;;
  putFlag)
	if [[ -z $2 || -z $3 || $(nodeExists $2) -eq 0 ]];then errorOutput "DEFINE NODE AND FLAG";exit;fi
    putFlag $2 $3
    ;;
  putAttachFlag)
	if [[ -z $2 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    putAttachFlag $2
    ;;
  dbStatus)
    dbStats
    ;;
  nodeStatusUpdate)
	if [[ -z $2 || -z $3 ]];then errorOutput "DEFINE NODE AND STATUS";exit;fi
    nodeStatusUpdate "$2" "$3"
    ;;
  detach)
	if [[ -z $2 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    detachNode "$2"
    ;;
  attach)
	if [[ -z $2 ]];then errorOutput "MUST CHOOSE A VALID NODE";exit;fi
    attachNode "$2"
    ;;
  status)
    status
    ;;
  ppIsUp)
    ppIsUp
    ;;
  restartPP)
    restartPP
    ;;
  nodeIntCon)
    nodeIntCon
    ;;
  restart)
    restartDaemon
    ;;
  findNodeId)
    findNodeId "$2"
    ;;
  checkPPStatus)
    checkPPStatus
    ;;
  clearFlags)
    clearFlags "$2"
    ;;
  sync)
	sync
    ;;
  restartServers)
	restartServers
    ;;
  multiMasterCheck)
	multiMasterCheck
    ;;
  xlogParse)
	xlogParse "$2"
    ;;
  establishSSHConnections)
	establishSSHConnections
    ;;	
  nodeDaemonRunning)
	nodeDaemonRunning "$2"
    ;;
  urgentError)
	errorOutput "$2" "urgent"
    ;;
  findCurrentMaster)
	findCurrentMaster
    ;;
  testNodeConnections)
	testNodeConnections
    ;;
  findMasterPP)
	findMasterPP
    ;;
  amIMaster)
	amIMaster
    ;;
  findYoungest)
	findYoungest "$2"
    ;;
  changeMaster)
    changeMaster "$2"
    ;;
  help)
  echo sync xlogParse findCurrentMaster multiMasterCheck putAttachFlag findYoungest kill unkill checkMaster nodes putFlag dbStatus status ppIsUp restartPP restart loggedMaster checkNodes checkPPStatus clearFlags detach attach clearAttachFlag nodeIntCon changeMaster
  # exit 1
esac
# checkMaster
# checkNodes
# ppIsUp
# checkServers
# nodeCount