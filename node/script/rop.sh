#!/bin/bash
SERVERS=()
POOLS=()
SELFIP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
SSHOPT=" -T  -o StrictHostKeyChecking=no "
CONFIGPATH="/root/replicads/conf"
LOGPATH="/root/replicads/log"
CONFPATH="/root/replicads/conf"
FLAGPATH="/root/replicads/flags"
DAEMONLOG="/var/log/replicads/psql.log"
MAINLOG="/var/log/replicads/REPLICADS_NODE.log"
CONFHOSTNAME="REPLICADSCONF"
SELFHOST=$(hostname)
me=$(whoami)
PPFLAGPATH="/root/replicads/ppflags"
URGENTEMAIL="ozgurkoy@gmail.com"
MAXCLONEFAILCOUNT=2
CLONEFAILRETRYTIMEOUT=60
OPTIMEOUT=100
SYNCOPTIMEOUT=300

#temporary
[[ ! -f "$DAEMONLOG" ]] && touch "$DAEMONLOG"
[[ ! -f "$MAINLOG" ]] && touch "$MAINLOG"
chmod 777 "$DAEMONLOG"
chmod 777 "$MAINLOG"

export TERM="xterm-256color"

sshCheck() {
	RETURN=0

	if [[ $(portcheck "$1" 22) -eq 0 ]];then
		echo 0
		return
	fi

	SSHCHECK=$(ssh root@$1 $SSHOPT -qo PasswordAuthentication=no echo 1 || echo 0)
	echo $SSHCHECK
}

portcheck() {
	if ! nc -w 5 -z $1 $2 2>/dev/null
	then
		echo "0"
	else
		echo "1"
		# exit
	fi
}

sendEmail(){
	TO="$1"
	SUBJECT="$2"
	CONTENT="$3"
	(echo -e "To:$TO\r\nSubject: REPLICADS-$SELFHOST $SUBJECT\r\n\r\n$CONTENT" |msmtp  -t $TO)&
}

errorOutput () {
	MSG=$(printf "$1")
	
	echo "[E][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 1)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
	
	if [[ $2 == "urgent" ]];then
		sendEmail "$URGENTEMAIL" "REPLICADS[$SELFHOST]-URGENT" "$MSG"
	fi
}

warningOutput () {
	MSG=$(printf "$1")

	echo "[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 3)[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
}

infoOutput () {

	MSG=$(printf "$1")
	echo "$(tput setaf 2)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"

	if  [[ ! -z $DOINFOLOG || (! -z $2 && $2 == "log") ]];then
		echo "[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	fi
}

recoveryQuery() {
	eff=`psql -t --username=repmgr --dbname=repmgr --host $1 -w -c "SELECT pg_is_in_recovery()"|& grep -P -i ^.+$`
	eff=$(echo $eff| sed -e 's/^ *//' -e 's/ *$//')

	echo $eff
}

replaceInFile(){
	host=$1
	file=$2
	replacement=$3
	keyplacement=$4
	[[ -z $4 ]] && keyplacement="replicads"

	# if grep -Fxq "#startreplicads" "$file"
	# then
	ssh $SSHOPT root@$host "[[ ! -f \"$file\" ]] && touch \"$file\";if grep -Fxq \"#start$keyplacement\" \"$file\"; then perl -0777 -i.original -pe \"s~#start$keyplacement.+?#end$keyplacement~#start$keyplacement\n$replacement\n#end$keyplacement~igs\" \"$file\"; else echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> \"$file\";fi"
	# else
	    # ssh $SSHOPT root@$host "echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> $file"
	# fi
}

isNumeric(){
	num=$1
	[ ! -z "${num##*[!0-9]*}" ] && echo 1 || echo 0
}

pathadd() {
	if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
		PATH="${PATH:+"$PATH:"}$1"
		#export $PATH
	fi
}

loadServers() {
	REGEX="^\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]"

	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        type=${BASH_REMATCH[2]}
			if [[ $type == "node" ]];then
				SERVERS+=($host)
			elif [[ $type == "pp" ]];then
				POOLS+=($host)
			fi
	   fi
	done
}

confHostIsReachable(){
	echo $(sshCheck $CONFHOSTNAME)
}

establishSSHConnections() {
	REGEX="^\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]"

	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        type=${BASH_REMATCH[2]}

			sshCon=$(ps aux | grep -P "ssh .+? [r]oot@$host$" | grep -Po "root\s+?\K([\d]+)")
			if [[ $(echo "$sshCon" | wc -l) -gt 1 ]];then
				echo "Problem with SSH connections, killing them all"
				pkill -f "ssh -N -T"
			fi

			[[ $(sshCheck $host) -eq 0 ]] && errorOutput "$host is not connectable" && continue

			if [[ ! $sshCon -gt 0 ]];then
				warningOutput "Connecting to $host for ssh bridge"
				nohup ssh -N $SSHOPT root@$host 2> /dev/null 1>&2 &
			else
				infoOutput "$host is connected to ssh"
			fi

	   fi
	done
}

isNumeric(){
	num=$1
	[ ! -z "${num##*[!0-9]*}" ] && echo 1 || echo 0
}

registerStandby() {
	master=$(searchCurrentMaster)
	
	clone "$master"
	start

	infoOutput "REGISTERING STANDBY"
	
	if [[ $(portcheck $master 5432) -eq 0 ]] ;then
		errorOutput "MASTER($master) CAN'T BE REACHED TO REGISTER STANDBY"
		return 
	fi
	
	eff=`su - postgres -c "repmgr -f /var/lib/postgresql/repmgr/repmgr.conf --verbose standby register"|& grep -P -i ^.+$`
	kerror=`expr "$eff" : '.*ERROR'`
	
	echo "STB REGISTER RESPONSE : $eff <<<<"
	
	if [[ $kerror -gt 0 ]]; then
		errorOutput "REGISTER STB ERROR"
	else
		infoOutput "REGISTER STB COMPLETE"
		delFlag
	fi
}

status() {
	allgood=1
	# for i in "${!SERVERS[@]}"
	# do
	# 	if [[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] ;then
	# 		echo "${SERVERS[$i]} can not be reached."
	# 		allgood=0
	# 		continue
	# 	fi
	# done

	if [[ $allgood -eq 0 ]];then
		errorOutput "NOT ALL SERVERS ARE CONNECTABLE, CANCELLING CLUSTER STATS"
		return 1
	fi

	infoOutput "STATUS"
	repmgr -f /var/lib/postgresql/repmgr/repmgr.conf cluster show
}

updateConfigHost(){
	REGEX="^\[(.+?)\]\[pp\]\[(.+?)\]\[(.+?)\]"

	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        weight=${BASH_REMATCH[2]}
	        ip=${BASH_REMATCH[3]}

			isMaster=$(ssh $SSHOPT root@$ip "[[ -f \"$PPFLAGPATH/isMaster\" ]] && echo \"good\"")

			if [[ $isMaster == "good" ]];then
				infoOutput "UPDATED CONFIGHOST TO : $host"
				HOSTS="\n$ip  $CONFHOSTNAME"
				replaceInFile "$SELFIP" "/etc/hosts" "$HOSTS" "REPCONHOST"
				break
			fi
	   fi
	done
}

findMasterPP(){
	REGEX="^\[(.+?)\]\[pp\]\[(.+?)\]\[(.+?)\]"

	for line in $(cat /root/replicads/conf/servers)
	do
	   if [[ $line =~ $REGEX ]]
	   then
	        host=${BASH_REMATCH[1]}
	        weight=${BASH_REMATCH[2]}
	        ip=${BASH_REMATCH[3]}

			isMaster=$(ssh $SSHOPT -q root@$ip "[[ -f \"$PPFLAGPATH/isMaster\" ]] && echo \"good\"")

			if [[ $isMaster == "good" ]];then
				echo $host
				return
			fi
	   fi
	done
	echo "none"
}

selfStatus() {
	[[ $(checkOp "selfStatus") -eq 1 ]] && errorOutput "ONGOING OPERATION(selfStatus), WON'T EXECUTE" && return

	# restartCount=$((restartCount+5))
	# restartCount=9494
	# export restartCount
	# echo ">>>$restartCount>>>"
	if [[ $(portcheck $SELFHOST 5432) -eq 0 ]] ;then
		errorOutput "ERROR:PSQL NOT RUNNING"
		clearOp "selfStatus"
		# putOp "skipIgnite"
	else
		eff=$(recoveryQuery "$SELFHOST")

		if [[ ! -f "/root/skip" && $eff == "t" ]];then
			infoOutput "RUNNING AS STANDBY"
		elif [[ $eff == "f" ]];then
			infoOutput "RUNNING AS MASTER"
		else
			restartSt=`expr "$eff" : '.*is starting up'`
			if [[ $restartSt -gt 0 || -f "/root/skip" ]]; then
				infoOutput "DATABASE STILL STARTING"
				restartCount=$(getRestartCount)
				infoOutput "CURRENT RESTART COUNT :::: $restartCount"

				if [[ $restartCount -gt 5 ]];then
					clearOp "skipIgnite"
					resetRestartCount
					stop
					start

				else
					echo "INCREMENTING"
					incrementRestartCount
					putOp "skipIgnite"
				fi
			else
				infoOutput "UNKNOWN STATE $eff"
			fi
		fi
	fi

	clearOp "selfStatus"
}

checkSlaveConn() {
	selfStatus
	eff=$(recoveryQuery "$SELFHOST")

	if [[ $eff == "t" ]];then
		warningOutput "CAN'T RUN ON STANDBY"
		return 0
	elif [[ $eff == "f" ]];then
    	for i in "${!SERVERS[@]}"
    	do
			if [[ "${SERVERS[$i]}" == "$SELFHOST" ]]; then
				continue;
			fi
			if [[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]];then
				errorOutput "NOTGOOD:[${SERVERS[$i]}]"
				continue
			fi

			eff=$(recoveryQuery "${SERVERS[$i]}")
			if [[ $eff != "t" ]]
			then
				errorOutput "NOTGOOD:[${SERVERS[$i]}]"
			fi
		done
	fi
}

searchCurrentMaster() {
	foundmaster="notfound"
	for i in "${!SERVERS[@]}"
	do
		[[ $(portcheck ${SERVERS[$i]} 5432) -eq 0 ]] && continue

		eff=$(recoveryQuery "${SERVERS[$i]}")
		if [[ $eff == "f" ]]
		then
			foundmaster=${SERVERS[$i]}
		fi
	done
	echo $foundmaster
}

loggedMaster() {
	echo $(ssh $SSHOPT root@$CONFHOSTNAME "tail -1 $CONFPATH/masters | head -1 | sed 's/\(^.*\)|.*/\1/'")
}

getLatestMaster() {
	latestMaster=$(loggedMaster)

	if [[ $latestMaster != $SELFHOST ]]
	then
		eff=$(recoveryQuery "$latestMaster")

		if [[ $eff == "f" ]];then
			#server is up
			echo $latestMaster
		else
			echo "notfound"
		fi

	else
		#we are the master
		echo $latestMaster
		# echo "notfound"
	fi
}

stop() {
	infoOutput "STOPDB "
	if [[ $(portcheck $SELFHOST 5432) -eq 0 && $(portcheck 127.0.0.1 5432) -eq 0 ]] ;then
		warningOutput "SERVER IS ALREADY DOWN"
		return 0
	fi
	if [[ $1 == "fast" ]];then
		param=" -m fast"
	else
		param=""
	fi

	if [[ $me == "postgres" ]]
	then
		pg_ctl -l $DAEMONLOG -D /data stop $param
	else
		su -l postgres -c "pg_ctl -l $DAEMONLOG -D /data stop $param" #&& checkMaster
	fi
}

selfStatusUpdate(){
	[[ $(sshCheck $CONFHOSTNAME) -eq 0 ]] && echo "halt" && return
	operation=$1
	detail=$2
	flag="$operation~$detail"
	infoOutput "PUT STATUS UPDATE FLAG $flag"
	# echo "ssh $SSHOPT root@$CONFHOSTNAME /root/replicads/sh/pp.sh nodeStatusUpdate $SELFHOST $flag"
	op=`ssh $SSHOPT root@$CONFHOSTNAME "/root/replicads/sh/pp.sh nodeStatusUpdate $SELFHOST $flag" |& grep -P -i ^.+$`
	# echo $op
}

getActivePP(){
	#get and set the latest pp from the server
	op=`ssh $SSHOPT root@$CONFHOSTNAME "/root/replicads/sh/pp.sh nodeStatusUpdate $SELFHOST $flag" |& grep -P -i ^.+$`
}

execFlag() {
	#we can get the flag from parameter
	flag=`ssh $SSHOPT -q root@$CONFHOSTNAME "[[ -f $FLAGPATH/$SELFHOST ]] && cat $FLAGPATH/$SELFHOST" |& grep -P -i ^.+$`
	if [[ ${#flag} -gt 0 ]];then
		operation=`expr "$flag" : '^\([^~]*\)'`
		detail=`expr "$flag" : '^.*~\(.*\)'`
		NOWT=$(date '+%s')
		case "$operation" in
			clone)
				infoOutput "CURRENT TRY COUNTER : $CLONETRYCOUNTER"
				if [[ $CLONETRYCOUNTER -gt $MAXCLONEFAILCOUNT ]];then
					REMAINTIME=$((NEXTFLAGEXECUTIONTIME-NOWT))
					
					if [[ $REMAINTIME -lt 0 ]];then
						warningOutput "WAITED FOR $CLONEFAILRETRYTIMEOUT SECONDS TO TRY CLONING, WILL RETRY NOW"
						CLONETRYCOUNTER=0
					else
						errorOutput "CLONE TRIES EXCEEDED MAX TRY COUNT($MAXCLONEFAILCOUNT). WILL RETRY CLONING IN $REMAINTIME SECONDS"
						return
					fi
					clearOp
				fi
				
				# selfStatusUpdate "clone" "$detail"
				stop "fast"
				cloned=$(clone "start")
				echo "CLONEDD::::"
				echo $cloned
				sleep 5
				effr=$(recoveryQuery "$SELFHOST")
				
				if [[ $(portcheck $SELFHOST 5432) -eq 0 || $effr != "t" ]] ;then
					errorOutput "CLONING FAILED:QUERY RESPONSE: $effr PORTCHECK:$(portcheck $SELFHOST 5432)"
					CLONETRYCOUNTER=$((CLONETRYCOUNTER+1))
					infoOutput "NEW TRY COUNTER : $CLONETRYCOUNTER"
					
					if [[ $CLONETRYCOUNTER -gt $MAXCLONEFAILCOUNT ]];then
						NEXTFLAGEXECUTIONTIME=$((LASTFLAGOPERATION+CLONEFAILRETRYTIMEOUT))
						errorOutput "CLONE TRIES EXCEEDED MAX TRY COUNT($MAXCLONEFAILCOUNT). WILL RETRY CLONING IN $CLONEFAILRETRYTIMEOUT SECONDS" "urgent"
						selfStatusUpdate "cloneFail"
						touch $FLAGPATH/restartDaemon
					fi
					
					return 0
				elif [[ `expr "$cloned" : '.*NO master found'` -eq 0 ]];then
					delFlag
					clearOp
					CLONETRYCOUNTER=0
				else
					errorOutput "WAITING FOR A MASTER TO COME UP TO BEGIN CLONING"
					clearOp
				fi
			;;
			kill)
				stop
				clearOp
			;;
			registerStandby)
				master=$(searchCurrentMaster)
				if [[ $master == "notfound" ]]
				then
					errorOutput "NO master found, CAN'T REGISTER STANDBY NOW"
				else
					#TODO check for fail
					warningOutput "Registering standby on master ($master)"
					# clone "$master"
					# start
					registerStandby
					# delFlag
				fi
			;;
			followMaster)
				folld=$(followMaster)
				echo "$folld @@@@@@"
				effr=$(recoveryQuery "$SELFHOST")

				if [[ $(portcheck $SELFHOST 5432) -eq 0 || $effr != "t" ]] ;then
					errorOutput "FOLLOWING might have failed, will retry"
					return 0
				elif [[ `expr "$folld" : '.*NO MASTER FOUND'` -gt 0 ]];then
					errorOutput "WAITING FOR A MASTER TO COME UP TO BEGIN FOLLOWING"
					clearOp
				elif [[ `expr "$folld" : '.*FOLLOWING FAILED'` -gt 0 ]];then
					errorOutput "FOLLOWING PROBABLY FAILED WILL RETRY"
					clearOp
				else
					delFlag
					clearOp
				fi
			;;
			restart)
				stop
				start
				sleep 4
				if [[ $(portcheck $SELFHOST 5432) -eq 0 ]] ;then
					errorOutput "Could not restart-not running. Will try again"
				else
					delFlag
				fi
				clearOp

			;;
			gomaster)
				stop
			;;
			promote)
				promote
				if [[ $(portcheck $SELFHOST 5432) -gt 0 ]] ;then
					delFlag
				fi
			;;
		esac
	fi
}

readFlag() {
	[[ $(portcheck $CONFHOSTNAME 22) -eq 0 ]] && echo "halt" && return

	flag=`ssh $SSHOPT -q root@$CONFHOSTNAME "[[ -f $FLAGPATH/$SELFHOST ]] && cat $FLAGPATH/$SELFHOST" |& grep -P -i ^.+$`
	if [[ ${#flag} -gt 0 ]];then
		operation=`expr "$flag" : '^\([^~]*\)'`
		detail=`expr "$flag" : '^.*~\(.*\)'`
		echo $operation
	else
		echo "noflag"
	fi
}

delFlag() {
	op=`ssh $SSHOPT root@$CONFHOSTNAME "[[ -f $FLAGPATH/$SELFHOST ]] && rm $FLAGPATH/$SELFHOST" |& grep -P -i ^.+$`
	selfStatusUpdate "good"
	infoOutput "FLAG DELETED FOR $SELFHOST"
}

checkOp(){
	if [[ -z $1 ]];then
		flagName="OPFLAG-nodeop"
	else
		flagName="OPFLAG-$1"
	fi

	if [[ -f "$FLAGPATH/$flagName" ]];then
		oldd=$(cat $FLAGPATH/$flagName)
		curd=$(date '+%s')
		# echo "$oldd -- $curd"
		if [[ $(isNumeric "$oldd") -eq 0 ]];then
			oldd=0
		fi

		if [[ $(($curd-$oldd)) -le $OPTIMEOUT ]];then
			# errorOutput "ONGOING OPERATION-NODE ($flagName), WON'T EXECUTE(NODE)";
			echo 1
			return
		# else
			# echo "Ooof"
			# echo $(($curd-$oldd))
		fi
	fi
	# echo "Opas $flagName"
	# date '+%s' >  "$FLAGPATH/$flagName"
	putOp $flagName
}

checkSyncOp(){
	if [[ -f "$FLAGPATH/syncop" ]];then
		oldd=$(cat $FLAGPATH/syncop)
		curd=$(date '+%s')

		if [[ $(isNumeric "$oldd") -eq 0 ]];then
			oldd=0
		fi

		if [[ $(($curd-$oldd)) -le $SYNCOPTIMEOUT ]];then
			echo 1
			return
		fi
	fi
	rm "$FLAGPATH/syncop"
}

putOp() {
	date '+%s' >  "$FLAGPATH/$1"
}

clearOp(){
	if [[ -z $1 ]];then
		flagName="nodeop"
	else
		flagName="$1"
	fi

	[[ -f "$FLAGPATH/OPFLAG-$flagName" ]] && rm "$FLAGPATH/OPFLAG-$flagName"
}

xlogLocation() {
	eff=`psql -t --username=repmgr --dbname=repmgr -w -c "SELECT
		pg_xlog_location_diff(
        (CASE
                WHEN (pg_is_in_recovery()) THEN
                        pg_last_xlog_receive_location()
                ELSE
                        pg_current_xlog_location()
        END),
        '000/00000000'
		) AS total_wal_offset "|& grep -P -i ^.+$`


	echo $eff
}

ignite() {
	if [[ -f "$FLAGPATH/skipIgnite" ]];then
		warningOutput "SKIPPING IGNITE"
		clearOp "skipIgnite"
		return 0
	fi

	[[ $(checkSyncOp) -eq 1 ]] && errorOutput "SKIPPING IGNITE - SYNCOP" && return
	[[ $(checkOp) -eq 1 ]] && errorOutput "ONGOING OPERATION(nodeop), WON'T EXECUTE" && return

	#try connecting to PP now.
	eff=$(recoveryQuery "$SELFHOST")

	flag=$(readFlag)
	infoOutput "CHECKING FLAGS"
	if [[ $flag == "halt" ]];then
		warningOutput "CAN'T REACH CONFIG SERVER HALTING"
		if [[ $(portcheck $SELFHOST 5432) -eq 0 ]];then
			start
		fi
	elif [[ $flag != "noflag" ]];then
		LASTFLAGOPERATION=$(date '+%s')
		warningOutput "FLAG FOUND : $flag"
		execFlag
	else
		LASTFLAGOPERATION=0
		infoOutput "NO FLAG FOUND"
		if [[ $eff == "t" ]];then
			infoOutput "ALREADY RUNNING AS STANDBY"
			clearOp
			return 0
		elif [[ $eff == "f" ]];then
			infoOutput "ALREADY RUNNING AS MASTER"
			clearOp
			return 0
		fi

		#not running, so check if it's the late master
		master=$(searchCurrentMaster)
		if [[ $master == "notfound" ]]
		then
			warningOutput "NO MASTER FOUND, CHECKING IF I AM THE LAST MASTER"
			#DECIDE ABOUT THIS
			lastMaster=$(loggedMaster)
			# echo $lastMaster
			# lastMaster=$(searchCurrentMaster)
			if [[ $lastMaster == $SELFHOST ]];then
				infoOutput "WELL I AM THE LAST MASTER"
				#self start
				start
				sleep 3
				promote
			else
				infoOutput "NOPE"
			fi
		else
			warningOutput "MASTER FOUND ($master) , CLONING"
			
			#TODO MOVE THE CLONE OPERATION INTO THE CLONE FUNCTION
			
			NOWT=$(date '+%s')
			infoOutput "CURRENT TRY COUNTER : $CLONETRYCOUNTER"
			if [[ $CLONETRYCOUNTER -gt $MAXCLONEFAILCOUNT ]];then
				REMAINTIME=$((NEXTFLAGEXECUTIONTIME-NOWT))
			
				if [[ $REMAINTIME -lt 0 ]];then
					warningOutput "WAITED FOR $CLONEFAILRETRYTIMEOUT SECONDS TO TRY CLONING, WILL RETRY NOW"
					CLONETRYCOUNTER=0
				else
					errorOutput "CLONE TRIES EXCEEDED MAX TRY COUNT($MAXCLONEFAILCOUNT). WILL RETRY CLONING IN $REMAINTIME SECONDS"
					return
				fi
				clearOp
			fi
			
			clone "start"
			
			sleep 5
			effr=$(recoveryQuery "$SELFHOST")
							
			if [[ $(portcheck $SELFHOST 5432) -eq 0 || $effr != "t" ]] ;then
				errorOutput "CLONING FAILED:QUERY RESPONSE: $effr PORTCHECK:$(portcheck $SELFHOST 5432)"
				CLONETRYCOUNTER=$((CLONETRYCOUNTER+1))
				infoOutput "NEW TRY COUNTER : $CLONETRYCOUNTER"
				
				if [[ $CLONETRYCOUNTER -gt $MAXCLONEFAILCOUNT ]];then
					NEXTFLAGEXECUTIONTIME=$((LASTFLAGOPERATION+CLONEFAILRETRYTIMEOUT))
					errorOutput "CLONE TRIES EXCEEDED MAX TRY COUNT($MAXCLONEFAILCOUNT). WILL RETRY CLONING IN $CLONEFAILRETRYTIMEOUT SECONDS" "urgent"
					selfStatusUpdate "cloneFail"
					touch $FLAGPATH/restartDaemon
				fi
				
				return 0
			fi
			
			
			
		fi
	fi
	clearOp
}

clone() {
    infoOutput "CLONE DB"
	
	if [[ ! -z $2 ]]
	then
		master=$2
	else
		master=$(searchCurrentMaster)
	fi

	if [[ $master == "notfound" ]]
	then
		errorOutput "NO master found to clone"
		infoOutput "CHECKING IF I AM THE LAST MASTER"
		lastMaster=$(loggedMaster)
		#!!!!!!!!TODO : DECIDE ABOUT HERE
		# if [[ $SELFHOST == $lastMaster ]];then
			# infoOutput "IT SEEMS THERE WAS A PROBLEM. NO NEW MASTER WAS REGISTERED AFTER ME. I AM GOING FOR MASTER AGAIN."
			# delFlag
		# fi
	else
		infoOutput "FOUND $master"
		[[ $(checkOp "cloneop") -eq 1 ]] && errorOutput "ONGOING OPERATION(cloneop), WON'T EXECUTE" && return
		selfStatusUpdate "clone" "$master"

		if [[ $me == "postgres" ]]
		then
			cloneOp=`repmgr -f /var/lib/postgresql/repmgr/repmgr.conf -D /data --force -d repmgr -p 5432 -U repmgr -R postgres --verbose standby clone $master|& grep -P -i ^.+$`
		else
			# su -l postgres -c "repmgr -f /var/lib/postgresql/repmgr/repmgr.conf -D /data --force -d repmgr -p 5432 -U repmgr -R postgres --verbose standby clone $master"
			cloneOp=`repmgr -f /var/lib/postgresql/repmgr/repmgr.conf -D /data --force -d repmgr -p 5432 -U repmgr -R postgres --verbose standby clone $master|& grep -P -i ^.+$`
		fi
		infoOutput "CLONEOPPP>>:"
		echo $cloneOp
		infoOutput "<<<<"
		kerror=`expr "$cloneOp" : '.*ERROR'`
		# kgood=`expr "$cloneOp" : '.*standby clone complete'`

		# if [[ $kgood -eq 0 && $kerror -gt 0 ]]; then
		if [[ $kerror -gt 0 ]]; then
			errorOutput "CLONE FAILED"
			clearOp "cloneop"
		else
			sleep 2
			infoOutput "CLONE SUCCESSFUL"
			if [[ $1 == "start" ]]
			then
				start
				sleep 3
				clearOp "cloneop"
			fi
		fi
	fi
}

start() {
	echo "STARTDB !! "
	if [[ $(portcheck $SELFHOST 5432) -gt 0 ]];then 
		echo "ALREADY RUNNING/STARTED"
		return
	fi
	
	if ! [[ -d "/var/run/postgresql/" ]]
	then
		mkdir /var/run/postgresql/
		chown postgres:postgres /var/run/postgresql/
	fi

	selfStatusUpdate "start"

	if [[ $me == "postgres" ]]
	then
		pg_ctl -l $DAEMONLOG -D /data restart
	else
		su -l postgres -c "pg_ctl -l $DAEMONLOG -D /data start" #&& checkMaster
	fi
	sleep 4

	if [[ $(portcheck $SELFHOST 5432) -gt 0 ]] ;then
		selfStatusUpdate "good"
	fi
}

remoteKill() {
	if [[ $(portcheck $1 5432) -eq 0 ]] ;then
		warningOutput "CANT REACH TO $1"
		return 0
	fi

	stopAction=`ssh $SSHOPT root@$1 "/root/replicads/sh/rop.sh faststop" |& grep -P -i ^.+$`
	echo $stopAction
}

promote() {
	infoOutput "PROMOTE"

	echo "checking self port"
	if [[ $(portcheck $SELFHOST 5432) -eq 0 ]] ;then
		warningOutput "SERVER IS DOWN STARTING"
		start
	fi
	if [[ $(portcheck $SELFHOST 5432) -eq 0 ]] ;then
		errorOutput "SERVER IS STILL DOWN."
		return
	fi
	selfStatusUpdate "promote"
	echo "SEARCING CURRENT MASTER"
	currentM=$(searchCurrentMaster)

	if [[ $currentM == $SELFHOST ]];then
		infoOutput "I am the current master fool"
		return 0
	fi


	if [[ $me == "postgres" ]]
	then
		repmgr -f /var/lib/postgresql/repmgr/repmgr.conf  -l $DAEMONLOG standby promote
	else
		echo "PROMOTING"

		if [[ $1 == "force" ]];then
			#means we must kill the current master.
			if [[ $currentM != "notfound" && $currentM != $SELFHOST ]];then
				warningOutput "Killing current master >> $currentM <<"
				stopAction=`ssh $SSHOPT root@$currentM "/root/replicads/sh/rop.sh faststop" |& grep -P -i ^.+$`
				infoOutput $stopAction
				sleep 4
			fi
		fi

		eff=`su -l postgres -c "repmgr -f /var/lib/postgresql/repmgr/repmgr.conf standby promote >> $DAEMONLOG" |& grep -P -i ^.+$`
		echo $eff
	fi

	kgood=`expr "$eff" : '.*STANDBY PROMOTE successful'`
	kerror=`expr "$eff" : '.*ERROR'`
	# sleep 4
	# checkMaster
	if [[ $kgood -eq 0 && $kerror -gt 0 ]]; then
		errorOutput "ERROR ON PROMOTION $kerror"

		errorOutput $eff
		return 0
	else
		selfStatusUpdate "good"
	fi

	if [[ $1 == "start" ]]
	then
		if [[ $kgood -gt 0 ]]; then
			start
		else
			errorOutput $eff
		fi
	fi

}

followMaster() {
	[[ $(checkOp "followop") -eq 1 ]] && errorOutput "ONGOING OPERATION(followop), WON'T EXECUTE" && return

	if [[ $(portcheck $SELFHOST 5432) -eq 0 ]] ;then
		start
	fi

	master=$(searchCurrentMaster)

	if [[ $master == "notfound" ]];then
		errorOutput "NO MASTER FOUND TO FOLLOW";
	else
		selfStatusUpdate "follow" "$master"
		eff=`su - postgres -c "cd /data;repmgr -f /var/lib/postgresql/repmgr/repmgr.conf --verbose standby follow"|& grep -P -i ^.+$`
		sleep 4
		kerror=`expr "$eff" : '.*error'`
		# effr=$(recoveryQuery "$SELFHOST")

		if [[ $(portcheck $SELFHOST 5432) -gt 0 && $kerror -eq 0 ]] ;then
			infoOutput "(F)FOLLOWMASTER, FOLLOWING SUCCEEDED"
			selfStatusUpdate "good"
			clearOp "followop"
		else
			errorOutput "FOLLOWING FAILED, WILL RETRY"
			clearOp "followop"
		fi
	fi
}

sendEmail(){
	TO="$1"
	SUBJECT="$2"
	CONTENT="$3"
	(echo -e "To:$TO\r\nSubject: REPLICADS-$SELFHOST $SUBJECT\r\n\r\n$CONTENT" |msmtp  -t $TO)&
}

#DEPRECATED
checkMaster(){
	infoOutput "RUNNNING CHECK MASTER ON $CONFHOSTNAME"
	ssh $SSHOPT root@$CONFHOSTNAME "/root/replicads/daemon.sh checkMaster"
}


pathadd "/usr/lib/postgresql/9.3/bin"
if [[ $1 != "establishSSHConnections" ]];then
	loadServers
fi
case "$1" in
                path)
pathadd "/usr/lib/postgresql/9.3/bin"
        ;;
                fservers)
searchCurrentMaster
        ;;
                ignite)
ignite
        ;;
                remoteStop)
remoteKill "$2"
        ;;
                status)
status
        ;;
                selfStatus)
selfStatus
        ;;
                stop)
stop
        ;;
                faststop)
stop "fast"
        ;;
                selfStatusUpdate)
selfStatusUpdate "$2"
        ;;
                recoveryQuery)
recoveryQuery "$2"
        ;;
                start)
start
        ;;
                checkSlaveConn)
checkSlaveConn
        ;;
                restart)
stop
start
        ;;
                clone)
stop "fast"
clone
        ;;
                clonestart)
stop "fast"
clone "start"
        ;;
                promote)
promote
        ;;
                xlogLocation)
xlogLocation
        ;;
                followMaster)
followMaster
        ;;
                updateConfigHost)
updateConfigHost
        ;;
                registerStandby)
registerStandby
        ;;
                establishSSHConnections)
establishSSHConnections
        ;;
                findMasterPP)
findMasterPP
        ;;
                promotestart)
promote "start"
        ;;
                confHostIsReachable)
confHostIsReachable
        ;;
                promoteforce)
promote "force"
        ;;
                *)
echo "Usage: start|stop|status"
        ;;
esac