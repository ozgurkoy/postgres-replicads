portcheck() {
	if ! nc -w 5 -z $1 $2 2>/dev/null
	then
		echo "0"
	else
		echo "1"
		# exit
	fi
}

sshCheck() {
	RETURN=0
	
	if [[ $(portcheck "$1" 22) -eq 0 ]];then
		echo 0
		return
	fi
	
	SSHCHECK=$(ssh root@$1 $SSHOPT -qo PasswordAuthentication=no echo 1 || echo 0)
	echo $SSHCHECK
}


readServerLine() {
	if [[ -z $2 ]];
	then
		ST0=$(cat $REPPATH/conf/servers)
	else
		# echo "oho"
		ST0=$(cat conf/servers|awk "/^\[.+?\]\[${2}\].+?$/")
	fi
	# echo "$ST0"
	# echo $(echo "${ST0}"|sed -n "${1}p"|perl -pe "s/\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]/$1 $2 $3 $4/")
	echo $(echo "${ST0}"|sed -n "${1}p"|gawk 'BEGIN{RS="[";ORS=" "};{gsub(/[\[\]]/,"");};$1')
}

findServerLine() {
	ST0=$(cat conf/servers|awk "/^\[.+?\]\[${2}\].+?$/")
	# echo "$ST0"
	# echo $(echo "${ST0}"|sed -n "${1}p"|perl -pe "s/\[(.+?)\]\[(.+?)\]\[(.+?)\]\[(.+?)\]/$1 $2 $3 $4/")
	echo $(echo "${ST0}"|grep -Pn \\[${1}\\]|grep -Po ^[0-9]+)
}

errorOutput () {
	MSG=$(printf "$1")
	
	echo "[E][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 1)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
	
	if [[ $2 == "urgent" ]];then
		sendEmail "$URGENTEMAIL" "REPLICADS[$SELFHOST]-URGENT" "$MSG"
	fi
}

warningOutput () {
	MSG=$(printf "$1")
	
	echo "[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	echo "$(tput setaf 3)[W][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
}

infoOutput () {

	MSG=$(printf "$1")
	echo "$(tput setaf 2)[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}$(tput sgr0)"
	
	if  [[ ! -z $DOINFOLOG || (! -z $2 && $2 == "log") ]];then
		echo "[I][$(date +'%y%m%d-%H%M')][$SELFHOST] ${MSG}" >> "$MAINLOG"
	fi
}
collectNodes (){
	n0=()
	for line in $(cat /root/replicads/conf/servers)
	do
	if [[ $line =~ $NODEREGEX ]]
	then
		match=${BASH_REMATCH[1]}
		
	fi
	done
}
replaceInFile(){
	host=$1
	file=$2
	replacement=$3
	keyplacement=$4
	[[ -z $4 ]] && keyplacement="replicads"
	
	# if grep -Fxq "#startreplicads" "$file"
	# then
	ssh $SSHOPT root@$host "[[ ! -f \"$file\" ]] && touch \"$file\";if grep -Fxq \"#start$keyplacement\" \"$file\"; then perl -0777 -i.original -pe \"s~#start$keyplacement.+?#end$keyplacement~#start$keyplacement\n$replacement\n#end$keyplacement~igs\" \"$file\"; else echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> \"$file\";fi"
	# else
	    # ssh $SSHOPT root@$host "echo -e \"#start$keyplacement\n$replacement\n#end$keyplacement\" >> $file"
	# fi
}
sendEmail(){
	TO="$1"
	SUBJECT="$2"
	CONTENT="$3"
	(echo -e "To:$TO\r\nSubject: REPLICADS-$SELFHOST $SUBJECT\r\n\r\n$CONTENT" |msmtp  -t $TO)&
}

isNumeric(){ 
	num=$1
	[ ! -z "${num##*[!0-9]*}" ] && echo 1 || echo 0
}

validIp()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    ([[ $stat -eq 1 ]] && echo 0) || echo 1
}


export -f portcheck
export -f errorOutput
export -f warningOutput
export -f infoOutput
export -f replaceInFile
export -f sendEmail
export -f isNumeric
export -f validIp